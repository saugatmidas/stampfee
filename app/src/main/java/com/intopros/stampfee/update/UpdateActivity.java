package com.intopros.stampfee.update;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.intopros.stampfee.BuildConfig;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.Goto;

import butterknife.BindView;
import butterknife.OnClick;

public class UpdateActivity extends AbstractActivity {

    @BindView(R.id.version)
    TextView version;
    @BindView(R.id.skip)
    TextView skip;

    @Override
    public int setLayout() {
        return R.layout.activity_update;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("IOE Preparation", false);
        version.setText("Current App Version : " + BuildConfig.VERSION_NAME);
        if(getIntent().getBooleanExtra("skip",true)){
            skip.setVisibility(View.VISIBLE);
        }else{
            skip.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.skip)
    public void Skip() {
        new Goto().home(getActivityContext());
        finish();
    }

    @OnClick(R.id.updatebtn)
    public void openPlaystore() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String packageName = getActivityContext().getPackageName();
        intent.setData(Uri.parse("market://details?id=" + packageName));
        if (!MyStartActivity(intent)) {
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?" + packageName));
            if (!MyStartActivity(intent)) {
                Toast.makeText(getActivityContext(), "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}
