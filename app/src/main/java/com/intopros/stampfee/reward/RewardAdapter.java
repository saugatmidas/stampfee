package com.intopros.stampfee.reward;

import android.Manifest;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.utils.Utilities;
import com.intopros.stampfee.utils.dialog.EditTextDialog;

import java.util.List;

public class RewardAdapter extends AbstractAdapter<StampTable> {

    String billno = "";
    String stamptype = "";

    public RewardAdapter(Activity a, List<StampTable> mlist) {
        this.activity = a;
        this.mItemList = mlist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_reward, viewGroup, false);
        return new RewardView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final RewardView holder = (RewardView) viewHolder;
        holder.setName(mItemList.get(position).getRestaurantname());
        holder.setaddress(mItemList.get(position).getAddress());
        holder.setDesc(mItemList.get(position).getDescription());
        holder.setRemaining(mItemList.get(position).getPending() + " Remains");
        holder.setRemaining("+ " + mItemList.get(position).getPending());
        holder.btn_claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stamptype=mItemList.get(position).getStamptype();
                if (mItemList.get(position).getBill()) {
                    final EditTextDialog input = new EditTextDialog(activity, null);
                    input.setTitle("Link reward to bill.");
                    input.btn_save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!TextUtils.isEmpty(input.getFeedback())) {
                                billno = input.getFeedback();
                                input.dialog.dismiss();
                                ActivityCompat.requestPermissions(activity, new
                                        String[]{Manifest.permission.CAMERA}, 24);
                            } else
                                Utilities.Toaster(activity, "Please enter bill number.");

                        }
                    });
                    input.setHint("Please enter bill number.");
                    input.setOk("Scan");
                    input.setClose();
                    input.show();
                } else {
                    ActivityCompat.requestPermissions(activity, new
                            String[]{Manifest.permission.CAMERA}, 24);
                }
            }
        });
    }

    public String getBillno() {
        return billno;
    }

    public String getStamptype() {
        return stamptype;
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
