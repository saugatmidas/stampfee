package com.intopros.stampfee.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.intopros.stampfee.BuildConfig;
import com.intopros.stampfee.base.AppController;

import java.util.List;


public class SyncDatabase {


    public String getUserid() {
        try {
            return AppController.getQuery(UserInfoTable.class).where(UserInfoTableDao.Properties.ApiToken.isNotNull()).limit(1).build().unique().getUserid();
        } catch (Exception e) {
            return "";
        }
    }

    public String getApiToken() {
        try {
            return AppController.getQuery(UserInfoTable.class).where(UserInfoTableDao.Properties.ApiToken.isNotNull()).limit(1).build().unique().getApiToken();
        } catch (Exception e) {
            return "";
        }
    }

    public UserInfoTable getUserData() {
        try {
            return AppController.getQuery(UserInfoTable.class).where(UserInfoTableDao.Properties.ApiToken.isNotNull()).build().list().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<RestroTable> getRestro(Integer type) {
        try {
            return AppController.getQuery(RestroTable.class).where(RestroTableDao.Properties.Type.eq(type)).build().list();
        } catch (Exception e) {
            return null;
        }
    }

    public List<DashboardTable> getDashboard() {
        return AppController.getQuery(DashboardTable.class).build().list();
    }

    public StampCountTable getStampCount() {
        return AppController.getQuery(StampCountTable.class).build().list().get(0);
    }

    public void increaseStamp(int count) {
        StampCountTable s = getStampCount();

        savefullStampCount(s.getUserid(), s.getStamps() + count, s.getReedems(), s.getRedeemavailable(), s.getOffersavailable());
    }

    public void decreaseRedems() {
        StampCountTable s = getStampCount();
        if (s.getRedeemavailable() > 0)
            savefullStampCount(s.getUserid(), s.getStamps() + 1, s.getReedems(), s.getRedeemavailable() - 1, s.getOffersavailable());
    }

    public void removeOffersCount() {
        StampCountTable s = getStampCount();
        savefullStampCount(s.getUserid(), s.getStamps() + 1, s.getReedems(), s.getRedeemavailable(), 0);
    }

    public void increaseReedem() {
        StampCountTable s = getStampCount();
        saveStampCount(s.getUserid(), s.getStamps(), s.getReedems() + 1);
    }

    public List<BannerTable> getBanner() {
        return AppController.getQuery(BannerTable.class).where(BannerTableDao.Properties.Expirydate.gt(System.currentTimeMillis())).build().list();
    }

    public void deleteDashboardTitle() {
        AppController.getQuery(DashboardTable.class).buildDelete().executeDeleteWithoutDetachingEntities();
    }

    public void saveDashboardTitle(List<DashboardTable> banner) {
        AppController.getDaoSession().getDashboardTableDao().insertOrReplaceInTx(banner);
    }

    public void deleteRestro() {
        AppController.getQuery(RestroTable.class).buildDelete().executeDeleteWithoutDetachingEntities();
    }

    public void deleteVideos() {
        AppController.getQuery(VideoTable.class).buildDelete().executeDeleteWithoutDetachingEntities();
    }

    public void saveRestro(List<RestroTable> banner) {
        AppController.getDaoSession().getRestroTableDao().insertOrReplaceInTx(banner);
    }

    public void saveVideos(List<VideoTable> videos) {
        AppController.getDaoSession().getVideoTableDao().insertOrReplaceInTx(videos);
    }

    public void saveUserData(UserInfoTable data) {
        AppController.getDaoSession().getUserInfoTableDao().insertOrReplaceInTx(data);
    }

    public void savefullStampCount(String userid, Integer stamps, Integer reedems, Integer redemsavailable, Integer offersavailable) {
        StampCountTable sc = new StampCountTable(userid, reedems, stamps);
        sc.setOffersavailable(offersavailable);
        sc.setRedeemavailable(redemsavailable);
        AppController.getDaoSession().getStampCountTableDao().insertOrReplaceInTx(sc);
    }

    public void saveStampCount(String userid, Integer reedems, Integer stamps) {
        StampCountTable sc = new StampCountTable(userid, reedems, stamps);
        AppController.getDaoSession().getStampCountTableDao().insertOrReplaceInTx(sc);
    }

    public void saveBanner(List<BannerTable> banner) {
        AppController.getDaoSession().getBannerTableDao().insertOrReplaceInTx(banner);
    }

    public List<VideoTable> getVideos() {
        return AppController.getQuery(VideoTable.class).limit(3).build().list();
    }

    public static void clearDatabase(Context context) {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(
                context.getApplicationContext(), BuildConfig.db, null);
        SQLiteDatabase db = devOpenHelper.getWritableDatabase();
        devOpenHelper.onUpgrade(db, 0, 0);
    }

}


