package com.intopros.stampfee.offerlist;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.response.OffersObject;

import java.util.List;
import java.util.Random;

public class OfferAdapter extends AbstractAdapter<OffersObject> {

    public static final int NO_IMAGE = 0;
    public static final int WITH_IMAGE = 1;


    public OfferAdapter(List<OffersObject> mlist) {
        this.mItemList = mlist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == WITH_IMAGE)
            return new OfferView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_offer, viewGroup, false));
        else if (viewType == NO_IMAGE)
            return new SimpleOfferView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_offer_simple, viewGroup, false));
        else
            return new OfferView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_offer, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof OfferView) {
            final OfferView holder = (OfferView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
            holder.setDesc(mItemList.get(position).getDescription());
            holder.setImage(mItemList.get(position).getImage());
            holder.setOfferType(mItemList.get(position).getOffertype());
            holder.setRestro(mItemList.get(position).getRestaurantname());
            holder.setTerms(mItemList.get(position).getTerms());
        }
        if (viewHolder instanceof SimpleOfferView) {
            final SimpleOfferView holder = (SimpleOfferView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
            holder.setDescription(mItemList.get(position).getDescription());
            holder.setImage(mItemList.get(position).getImage());
            holder.setOfferType(mItemList.get(position).getOffertype());

            int[] androidColors = holder.rootView.getResources().getIntArray(R.array.colors);
            int randomAndroidColor = androidColors[position % 19];
            holder.ivColorImage.setBackgroundColor(randomAndroidColor);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
