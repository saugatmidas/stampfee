package com.intopros.stampfee.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

@Entity(indexes = { @Index(value = "restaurantid,type", unique = true)})
public class RestroTable {

    @SerializedName("restaurantname")
    @Expose
    private String restaurantname;
    @SerializedName("restaurantid")
    @Expose
    private Integer restaurantid;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("longitude")
    @Expose
    private float longitude;
    @SerializedName("latitude")
    @Expose
    private float latitude;
    @SerializedName("stamps")
    @Expose
    private String stamps;
    @SerializedName("redeems")
    @Expose
    private String redeems;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("hasoffer")
    @Expose
    private Boolean hasoffer;
    @SerializedName("images")
    @Expose
    private String images = null;

    @Generated(hash = 1920753489)
    public RestroTable(String restaurantname, Integer restaurantid, Integer type,
            String address, String phone, float longitude, float latitude,
            String stamps, String redeems, String website, String description,
            String logo, Boolean hasoffer, String images) {
        this.restaurantname = restaurantname;
        this.restaurantid = restaurantid;
        this.type = type;
        this.address = address;
        this.phone = phone;
        this.longitude = longitude;
        this.latitude = latitude;
        this.stamps = stamps;
        this.redeems = redeems;
        this.website = website;
        this.description = description;
        this.logo = logo;
        this.hasoffer = hasoffer;
        this.images = images;
    }
    @Generated(hash = 992578409)
    public RestroTable() {
    }

    public String getRestaurantname() {
        return this.restaurantname;
    }
    public void setRestaurantname(String restaurantname) {
        this.restaurantname = restaurantname;
    }
    public Integer getRestaurantid() {
        return this.restaurantid;
    }
    public void setRestaurantid(Integer restaurantid) {
        this.restaurantid = restaurantid;
    }
    public Integer getType() {
        return this.type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return this.phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public float getLongitude() {
        return this.longitude;
    }
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
    public float getLatitude() {
        return this.latitude;
    }
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
    public String getStamps() {
        return this.stamps;
    }
    public void setStamps(String stamps) {
        this.stamps = stamps;
    }
    public String getRedeems() {
        return this.redeems;
    }
    public void setRedeems(String redeems) {
        this.redeems = redeems;
    }
    public String getWebsite() {
        return this.website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }
    public String getImages() {
        return this.images;
    }
    public void setImages(String images) {
        this.images = images;
    }
    public Boolean getHasoffer() {
        return this.hasoffer;
    }
    public void setHasoffer(Boolean hasoffer) {
        this.hasoffer = hasoffer;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getLogo() {
        return this.logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }
}