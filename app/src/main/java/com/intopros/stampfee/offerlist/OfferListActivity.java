package com.intopros.stampfee.offerlist;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.JsonObject;
import com.gturedi.views.StatefulLayout;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.response.OffersObject;

import java.util.ArrayList;

import butterknife.BindView;

public class OfferListActivity extends AbstractActivity {

    @BindView(R.id.swipe_reload)
    SwipeRefreshLayout swipe_reload;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.stateful)
    StatefulLayout stateful;

    OfferAdapter mAdapter;

    ArrayList<OffersObject> mlist = new ArrayList<>();

    @Override
    public int setLayout() {
        return R.layout.activity_offer_list;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Offers", true);
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        mAdapter = new OfferAdapter(mlist);
        recycler_view.setAdapter(mAdapter);

        swipe_reload.post(new Runnable() {
            @Override
            public void run() {
                swipe_reload.setRefreshing(true);
                getOffers();
            }
        });

        swipe_reload.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOffers();
            }
        });
    }


    private void getOffers() {
        new SetRequest().get(getActivityContext())
                .set(ServiceGenerator.createService().offers())
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
                        swipe_reload.setRefreshing(false);
                        ResponseClass.OffersResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.OffersResponse.class);
                        mlist.clear();
                        mlist.addAll(response.getResponse());
                        mAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        swipe_reload.setRefreshing(false);
                        if (mlist.isEmpty()) {
                            if (code != 204) {
                                stateful.showError(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        swipe_reload.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                swipe_reload.setRefreshing(true);
                                                getOffers();
                                            }
                                        });
                                    }
                                });
                            }else{
                                stateful.showEmpty("No Offers Available");
                            }
                        }
                    }
                });

    }
}
