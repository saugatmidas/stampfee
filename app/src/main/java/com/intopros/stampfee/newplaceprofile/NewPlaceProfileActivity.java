package com.intopros.stampfee.newplaceprofile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.dashboard.slider.FlatSliderAdapter;
import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.imageviewer.ImageViewActivity;
import com.intopros.stampfee.placeprofile.PlaceProfileAdapter;
import com.intopros.stampfee.profile.StampDetailDialog;
import com.intopros.stampfee.response.PlaceProfileObject;
import com.intopros.stampfee.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;

import butterknife.BindView;
import ss.com.bannerslider.Slider;
import ss.com.bannerslider.event.OnSlideClickListener;

public class NewPlaceProfileActivity extends AbstractActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.imgslider)
    Slider imgslider;
    @BindView(R.id.layout_toolbar)
    ConstraintLayout layoutToolbar;

    RestroTable restro;

    @BindView(R.id.profilelist)
    RecyclerView profileList;

    OffersFragment offersFragment;
    RewardsFragment rewardsFragment;
    ViewPagerAdapter adapter;

    @Override
    public int setLayout() {
        return R.layout.activity_new_place_profile;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        restro = ServiceGenerator.getGson().fromJson(getIntent().getStringExtra("restro"), RestroTable.class);
        setPageTitle("Stampfee", true);
        if (!TextUtils.isEmpty(restro.getImages()))
            setImages(restro.getImages().split(","));
        else
            layoutToolbar.setBackground(ContextCompat.getDrawable(getActivityContext(), R.color.themecolor));

        loadData();
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(adapter.getCount());
        viewPager.setAdapter(adapter);
        requestData();
    }

    private void loadData() {
        ArrayList<HomeModel> mlist = new ArrayList<>(4);
        mlist.add(new HomeModel("profile"));
        mlist.add(new HomeModel("color", "Call", "call", restro.getPhone() != null ? R.color.g_green : R.color.icon_grey, R.drawable.ic_call_black_24dp));
        mlist.add(new HomeModel("color", "Locate", "map", restro.getLatitude() != 0 ? R.color.g_blue : R.color.icon_grey, R.drawable.ic_pin_drop_black_24dp));
        mlist.add(new HomeModel("color", "Website", "fb", restro.getWebsite() != null ? R.color.com_facebook_button_background_color : R.color.icon_grey, R.drawable.ic_web_black_24dp));

        final PlaceProfileAdapter adapter;
        GridLayoutManager layoutManager;

        layoutManager = new GridLayoutManager(getActivityContext(), 3);
        adapter = new PlaceProfileAdapter(this, mlist);
        adapter.setStampCountVisibilityForProfile(false);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case 7:
                    case 2:
                        return 1;
                    default:
                        return 3;
                }
            }
        });

        profileList.setLayoutManager(layoutManager);
        profileList.setAdapter(adapter);
    }

    private void requestData() {
        tabLayout.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        new SetRequest().get(getActivityContext()).pdialog(false).set(ServiceGenerator.createService()
                .restroprofile(restro.getRestaurantid())).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                ResponseClass.PlaceProfileResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.PlaceProfileResponse.class);
                if (!Utilities.isListEmpty(response.getResponse().getOffers()) || Utilities.isListEmpty(response.getResponse().getRedeem())) {
                    setupViewPager(viewPager, response.getResponse());
                }

            }

            @Override
            public void OnError(String err, String errcode, int code, JsonObject res) {
//                errorblock.setVisibility(View.VISIBLE);
//                errmsg.setText(err);
            }
        });

    }

    private void setupViewPager(ViewPager viewPager, PlaceProfileObject response) {
        tabLayout.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);

        if (!Utilities.isListEmpty(response.getOffers())) {
            if (offersFragment == null) {
                offersFragment = new OffersFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("list", ServiceGenerator.getGson().toJson(response.getOffers()));
                offersFragment.setArguments(bundle1);
                adapter.addFragment(offersFragment, "Offers");
                adapter.notifyDataSetChanged();
            } else {
                offersFragment.setupOfferList(response.getOffers());
            }
        } else {
            if (offersFragment != null) {
                adapter.deleteFragment(offersFragment);
            }
        }

        if (!Utilities.isListEmpty(response.getRedeem())) {
            if (rewardsFragment == null) {
                rewardsFragment = new RewardsFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putString("list", ServiceGenerator.getGson().toJson(response.getRedeem()));
                Utilities.Logger("redeemList: " + ServiceGenerator.getGson().toJson(response.getRedeem()));
                rewardsFragment.setArguments(bundle2);
                adapter.addFragment(rewardsFragment, "My Cards");
                adapter.notifyDataSetChanged();
            } else {
                rewardsFragment.setupRedeemList(response.getRedeem());
            }
        } else {
            if (rewardsFragment != null) {
                adapter.deleteFragment(rewardsFragment);
            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return FragmentPagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void deleteFragment(Fragment fragment) {
            for (int i = 0; i < mFragmentList.size(); i++) {
                Fragment item = mFragmentList.get(i);
                if (item.getClass().equals(fragment.getClass())) {
                    mFragmentList.remove(i);
                    mFragmentTitleList.remove(i);
                    destroyItem(viewPager, i, fragment);
                    notifyDataSetChanged();
                    break;
                }
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void setImages(final String[] mlist) {
        imgslider.setAdapter(new FlatSliderAdapter(mlist));
        if (mlist.length > 0) {
            imgslider.setOnSlideClickListener(new OnSlideClickListener() {
                @Override
                public void onSlideClick(int position) {
                    try {
                        startActivity(new Intent(getActivityContext(), ImageViewActivity.class)
                                .putExtra("image", mlist[position]));
                    } catch (Exception e) {
                        startActivity(new Intent(getActivityContext(), ImageViewActivity.class)
                                .putExtra("image", mlist[0]));
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == StampDetailDialog.STAMP_DIALOG)
            if (resultCode == RESULT_OK)
                requestData();

    }
}
