package com.intopros.stampfee.profile;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StampIconView extends RecyclerView.ViewHolder {

    View rView;

    @BindView(R.id.ricon)
    ImageView ricon;

    public StampIconView(View view) {
        super(view);
        this.rView = view;
        ButterKnife.bind(this, rView);
    }
}
