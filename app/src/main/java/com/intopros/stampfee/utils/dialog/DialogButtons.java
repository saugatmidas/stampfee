package com.intopros.stampfee.utils.dialog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DialogButtons {
    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("cancel")
    @Expose
    private Boolean cancel;
    @SerializedName("buttontext")
    @Expose
    private String buttontext;
    @SerializedName("action")
    @Expose
    private Object action;
    @SerializedName("url")
    @Expose
    private Object url;

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public Boolean getCancel() {
        return cancel;
    }

    public void setCancel(Boolean cancel) {
        this.cancel = cancel;
    }

    public String getButtontext() {
        return buttontext;
    }

    public void setButtontext(String buttontext) {
        this.buttontext = buttontext;
    }

    public Object getAction() {
        return action;
    }

    public void setAction(Object action) {
        this.action = action;
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }
}
