package com.intopros.stampfee.dashboard;

import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.database.VideoTable;
import com.intopros.stampfee.response.OffersObject;

import java.util.List;

public class HomeModel {
    String type;
    String title;
    String id;
    int color;
    int icon;
    List<RestroTable> restrolist;
    List<VideoTable> videolist;
    RestroTable restro;
    StampTable stamp;
    OffersObject offer;

    public HomeModel(String type) {
        this.type = type;
    }

    public HomeModel(String type, OffersObject offer) {
        this.type = type;
        this.offer = offer;
    }

    public HomeModel(String type, StampTable satmp) {
        this.type = type;
        this.stamp = satmp;
    }

    public HomeModel(String type, String title, List<RestroTable> restrolist) {
        this.type = type;
        this.title = title;
        this.restrolist = restrolist;
    }

    public HomeModel(String type, List<VideoTable> videolist, String title) {
        this.type = type;
        this.title = title;
        this.videolist = videolist;
    }

    public HomeModel(String type, RestroTable restro) {
        this.type = type;
        this.restro = restro;
    }

    public HomeModel(String type, String title, int color, int icon) {
        this.type = type;
        this.title = title;
        this.color = color;
        this.icon = icon;
    }

    public HomeModel(String type, String title, String id, int color, int icon) {
        this.type = type;
        this.title = title;
        this.id = id;
        this.color = color;
        this.icon = icon;
    }

    public OffersObject getOffer() {
        return offer;
    }

    public void setOffer(OffersObject offer) {
        this.offer = offer;
    }

    public StampTable getStamp() {
        return stamp;
    }

    public void setStamp(StampTable stamp) {
        this.stamp = stamp;
    }

    public List<RestroTable> getRestrolist() {
        return restrolist;
    }

    public void setRestrolist(List<RestroTable> restrolist) {
        this.restrolist = restrolist;
    }

    public RestroTable getRestro() {
        return restro;
    }

    public void setRestro(RestroTable restro) {
        this.restro = restro;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public HomeModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<VideoTable> getVideolist() {
        return videolist;
    }

    public void setVideolist(List<VideoTable> videolist) {
        this.videolist = videolist;
    }
}
