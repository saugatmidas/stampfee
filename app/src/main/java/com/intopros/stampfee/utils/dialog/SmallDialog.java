package com.intopros.stampfee.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.intopros.stampfee.R;

public class SmallDialog {
    Dialog dialog;
    Button dialogCButton,dialogButton;
    TextView text;
    OnDialogSelect cl=null;

    public SmallDialog(Context a, String m, OnDialogSelect c){
         dialog = new Dialog(a);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_small);
        text = (TextView) dialog.findViewById(R.id.text);
        dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogCButton = (Button) dialog.findViewById(R.id.dialogButtonCancle);

        text.setText(m);
        cl=c;

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(cl!=null)
                cl.onSuccess();
            }
        });
        dialogCButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(cl!=null)
                cl.onCancel();
            }
        });

    }

    public SmallDialog hideok(){
        dialogButton.setVisibility(View.GONE);
        return this;
    }
    public SmallDialog hideno(){
        dialogCButton.setVisibility(View.GONE);
        return this;
    }

    public SmallDialog setCancelable(Boolean b){
        dialog.setCancelable(b);
        dialog.setCanceledOnTouchOutside(b);
        return this;
    }

    public SmallDialog show(){
        dialog.show();
        return this;
    }

    public SmallDialog setmsg(String s){
        text.setText(s);
        return this;
    }
    public SmallDialog setyes(String s){
        dialogButton.setText(s);
        return this;
    }
    public SmallDialog setno(String s){
        dialogCButton.setText(s);
        return this;
    }
}