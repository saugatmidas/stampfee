package com.intopros.stampfee.reward;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.JsonObject;
import com.gturedi.views.StatefulLayout;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.qrreader.BarcodeReaderActivity;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class RewardActivity extends AbstractActivity {

    private final int CLAIM = 199;

    @BindView(R.id.swipe_reload)
    SwipeRefreshLayout swipe_reload;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.stateful)
    StatefulLayout stateful;

    RewardAdapter mAdapter;

    ArrayList<StampTable> mlist = new ArrayList<>();

    @Override
    public int setLayout() {
        return R.layout.activity_reward;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Rewards", true);
        recycler_view.setLayoutManager(new GridLayoutManager(getActivityContext(), 1));
        mAdapter = new RewardAdapter(this, mlist);
        recycler_view.setAdapter(mAdapter);

        OverScrollDecoratorHelper.setUpOverScroll(recycler_view, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        swipe_reload.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestData();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        swipe_reload.post(new Runnable() {
            @Override
            public void run() {
                swipe_reload.setRefreshing(true);
                requestData();
            }
        });
    }

    private void requestData() {
        stateful.showContent();
        mlist.clear();
        new SetRequest().get(getActivityContext()).set(ServiceGenerator.createService()
                .getprofile("rewards")).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                swipe_reload.setRefreshing(false);
                ResponseClass.ProfileResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.ProfileResponse.class);
                loadData(response.getResponse());
            }

            @Override
            public void OnError(String err, String errcode, int code, final JsonObject res) {
                swipe_reload.setRefreshing(false);
                if (mlist.isEmpty()) {
                    if (code != 204) {
                        stateful.showError(err, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                swipe_reload.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipe_reload.setRefreshing(true);
                                        requestData();
                                    }
                                });
                            }
                        });
                    } else {
                        stateful.showEmpty("Scan more to avail rewards.");
                    }
                } else {
                    if (code == 204) {
                        mlist.clear();
                        mAdapter.notifyDataSetChanged();
                    }
                }

            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 24:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /* takePicture(); */
                    Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(getActivityContext(), true, false);
                    startActivityForResult(launchIntent, CLAIM);
                } else {
                    Toast.makeText(getActivityContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == CLAIM && data != null) {
            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
            scanClaim(barcode.rawValue);
        }
    }

    private void scanClaim(String code) {
        new SetRequest().get(getActivityContext()).pdialog(false)
                .set(ServiceGenerator.createService().redeem(mAdapter.getStamptype(), mAdapter.getBillno(), code, getPref(SharedValue.currentLong), getPref(SharedValue.currentLat)))
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
//                        new SyncDatabase().decreaseRedems();
                        swipe_reload.post(new Runnable() {
                            @Override
                            public void run() {
                                swipe_reload.setRefreshing(true);
                                requestData();
                            }
                        });
                        ResponseClass.StringResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.StringResponse.class);
                        new SmallDialog(getActivityContext(), response.getMessage(), null).hideno().setyes("OK").show();
                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        new SmallDialog(getActivityContext(), err, null).show();

                    }
                });
    }

    private void loadData(List<StampTable> response) {
        mlist.clear();
        mlist.addAll(response);
        mAdapter.notifyDataSetChanged();
    }
}
