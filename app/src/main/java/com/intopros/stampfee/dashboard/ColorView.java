package com.intopros.stampfee.dashboard;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ColorView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.badge)
    TextView badge;
    @BindView(R.id.menutitle)
    TextView menutitle;
    @BindView(R.id.menuicon)
    ImageView menuicon;

    public ColorView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setTitle(String txt) {
        menutitle.setText(txt);
    }

    public void setBadge(int count) {
        if(count<1){
            hideBadge();
        }else if( count>9) {
            badge.setText(count+"+");
        }else{
            badge.setText(count+"");
        }
    }

    public void hideBadge() {
        badge.setVisibility(View.GONE);
    }

    public void setBackground(int color) {
//        ViewCompat.setBackgroundTintList(cntn,ColorStateList.valueOf(rView.getResources().getColor(color)));
        ColorStateList csl = AppCompatResources.getColorStateList(rView.getContext(), color);
        Drawable drawable = DrawableCompat.wrap(menuicon.getBackground());
        DrawableCompat.setTintList(drawable, csl);
        menuicon.setBackground(drawable);
        menutitle.setTextColor(ContextCompat.getColor(rView.getContext(), color));
    }

    public void setIcon(int icon) {
        menuicon.setImageDrawable(ResourcesCompat.getDrawable(rView.getResources(), icon, null));

    }
}
