package com.intopros.stampfee.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;

public class PermissionUtils {

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean neverAskAgainSelected(final Activity activity, final String permission) {
        final boolean prevShouldShowStatus = getRatinaleDisplayStatus(activity, permission);
        final boolean currShouldShowStatus = activity.shouldShowRequestPermissionRationale(permission);
        return prevShouldShowStatus != currShouldShowStatus;
    }

    public static void setShouldShowStatus(final Context context, final String permission) {
        SharedPreferencesHelper.setSharedPreferences(context, SharedValue.openSettingForLocation, true);
    }

    public static boolean getRatinaleDisplayStatus(final Context context, final String permission) {
        return SharedPreferencesHelper.getSharedPreferences(context, SharedValue.openSettingForLocation, false);
    }
}