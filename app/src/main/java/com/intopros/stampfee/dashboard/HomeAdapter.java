package com.intopros.stampfee.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.HomeActivity;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.dashboard.featuredcafe.FeaturedView;
import com.intopros.stampfee.dashboard.featuredcafe.HorizontalAdapter;
import com.intopros.stampfee.dashboard.slider.ImageSliderView;
import com.intopros.stampfee.dashboard.stamps.RestroStampView;
import com.intopros.stampfee.dashboard.stamps.TitleView;
import com.intopros.stampfee.dashboard.videorecipe.HorizontalRecipeAdapter;
import com.intopros.stampfee.dashboard.videorecipe.VideoRecipeView;
import com.intopros.stampfee.database.StampCountTable;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.newplaceprofile.NewPlaceProfileActivity;
import com.intopros.stampfee.offerlist.OfferListActivity;
import com.intopros.stampfee.profile.ProfileActivity;
import com.intopros.stampfee.reward.RewardActivity;
import com.intopros.stampfee.searchplaces.SearchPlacesActivity;
import com.intopros.stampfee.video.VideoListActivity;

import java.util.List;


public class HomeAdapter extends AbstractAdapter<HomeModel> {

    public final int SLIDER = 1;
    public final int FEATURE = 5;
    public final int COLOR = 2;
    public final int TITLE = 6;
    public final int CIRCLE = 7;
    public final int RESTROSTAMP = 3;
    public final int VIDEO = 8;
    public final int LOADING = 4;

    SyncDatabase syncdb;

    public HomeAdapter(Activity a, List<HomeModel> mlist) {
        this.activity = a;
        this.mItemList = mlist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TITLE:
                return new TitleView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_title, parent, false));
            case LOADING:
                return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false));
            case SLIDER:
                return new ImageSliderView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_slider, parent, false));
            case FEATURE:
                return new FeaturedView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recyclerview, parent, false));
            case COLOR:
                return new ColorView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_color, parent, false));
            case CIRCLE:
                return new CircleView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_circle, parent, false));
            case RESTROSTAMP:
                return new RestroStampView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_restro_stamp, parent, false));
            case VIDEO:
                return new VideoRecipeView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recyclerview, parent, false));
            default:
                return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);
        syncdb = new SyncDatabase();
        if (viewHolder instanceof ImageSliderView) {
            ImageSliderView holder = (ImageSliderView) viewHolder;
            holder.setItem(new SyncDatabase().getBanner());

        } else if (viewHolder instanceof FeaturedView) {
            FeaturedView holder = (FeaturedView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
            holder.setAdapter(new HorizontalAdapter(activity, mItemList.get(position).getRestrolist()));
            holder.h_recycler.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            holder.viewall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity, SearchPlacesActivity.class)
                            .putExtra("type", mItemList.get(position).getRestrolist().get(0).getType() + "")
                            .putExtra("title", mItemList.get(position).getTitle())
                    );
                }
            });

        } else if (viewHolder instanceof TitleView) {
            TitleView holder = (TitleView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
        } else if (viewHolder instanceof CircleView) {
            CircleView holder = (CircleView) viewHolder;
            holder.setTitle(mItemList.get(position).getRestro().getRestaurantname());
            holder.setRandomColor(position);
            holder.setPlaceholder(mItemList.get(position).getRestro().getRestaurantname(), mItemList.get(position).getRestro().getLogo());


            holder.rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity, NewPlaceProfileActivity.class).putExtra("restro", ServiceGenerator.getGson().toJson(mItemList.get(position).getRestro())));
                }
            });
        } else if (viewHolder instanceof RestroStampView) {
            RestroStampView holder = (RestroStampView) viewHolder;
            holder.tv_cafe_name.setText(mItemList.get(position).getRestro().getRestaurantname());
            holder.tv_cafe_location.setText(mItemList.get(position).getRestro().getAddress());
            holder.tv_redeem.setText(mItemList.get(position).getRestro().getRedeems() + " Redeems");
            holder.tv_stamps.setText(mItemList.get(position).getRestro().getStamps() + " Stamps");

            if (mItemList.get(position).getRestro().getImages() != null)
                holder.setImage(mItemList.get(position).getRestro().getImages().split(",")[0]);

            holder.rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity, NewPlaceProfileActivity.class).putExtra("restro", ServiceGenerator.getGson().toJson(mItemList.get(position).getRestro())));
                }
            });

        } else if (viewHolder instanceof ColorView) {
            ColorView holder = (ColorView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
            holder.setBackground(mItemList.get(position).getColor());
            holder.setIcon(mItemList.get(position).getIcon());

            StampCountTable count = syncdb.getStampCount();
            switch (mItemList.get(position).getId().trim().toLowerCase()) {
                case "offer":
//                    Log.d("counttcheck","count--offer--"+count.getOffersavailable());

                    holder.setBadge(count.getOffersavailable());
                    break;
                case "reward":
//                    Log.d("counttcheck","count--reward--"+count.getRedeemavailable());

                    holder.setBadge(count.getRedeemavailable());
                    break;
                default:
                    holder.hideBadge();
            }

            holder.rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (mItemList.get(position).getId().trim().toLowerCase()) {
                        case "offer":
                            activity.startActivity(new Intent(activity, OfferListActivity.class));
                            break;
                        case "profile":
                            activity.startActivity(new Intent(activity, ProfileActivity.class));
                            break;
                        case "places":
                            activity.startActivity(new Intent(activity, SearchPlacesActivity.class));
                            break;
                        case "scan":
                            ((HomeActivity) activity).scanCode();
                            break;
                        case "reward":
                            activity.startActivity(new Intent(activity, RewardActivity.class));
                            break;
                    }
                }
            });
        } else if (viewHolder instanceof VideoRecipeView) {
            VideoRecipeView holder = (VideoRecipeView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
            holder.setAdapter(new HorizontalRecipeAdapter(activity, mItemList.get(position).getVideolist()));
            holder.h_recycler.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            holder.viewall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity, VideoListActivity.class));
                }
            });

        }
        viewHolder.setIsRecyclable(false);
    }

    @Override
    public int getItemViewType(int position) {
        switch (mItemList.get(position).getType()) {
            case "title":
                return TITLE;
            case "loading":
                return LOADING;
            case "slider":
                return SLIDER;
            case "circle":
                return CIRCLE;
            case "feature":
                return FEATURE;
            case "restrostamp":
                return RESTROSTAMP;
            case "color":
                return COLOR;
            case "video":
                return VIDEO;
            default:
                return LOADING;
        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
