package com.intopros.stampfee.base.baseutils.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.intopros.stampfee.database.BannerTable;

import java.util.List;

public class ExtraParamObject {

    @SerializedName("minversion")
    @Expose
    private Integer minversion;
    @SerializedName("forceupdate")
    @Expose
    private String forceupdate;
    @SerializedName("reedems")
    @Expose
    private Integer reedems;
    @SerializedName("stamps")
    @Expose
    private Integer stamps;
    @SerializedName("updatedata")
    @Expose
    private Boolean update;

    @SerializedName("nextpage")
    @Expose
    private String nextpage;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("showdialog")
    @Expose
    private Boolean showdialog;


    @SerializedName("offersavailable")
    @Expose
    private Integer offersavailable;
    @SerializedName("redeemavailable")
    @Expose
    private Integer redeemavailable;


    @SerializedName("banners")
    @Expose
    private List<BannerTable> banners = null;

    public List<BannerTable> getBanners() {
        return banners;
    }


    public Integer getOffersavailable() {
        return offersavailable;
    }

    public void setOffersavailable(Integer offersavailable) {
        this.offersavailable = offersavailable;
    }

    public Integer getRedeemavailable() {
        return redeemavailable;
    }

    public void setRedeemavailable(Integer redeemavailable) {
        this.redeemavailable = redeemavailable;
    }

    public Integer getReedems() {
        return reedems;
    }

    public void setReedems(Integer reedems) {
        this.reedems = reedems;
    }

    public Integer getStamps() {
        return stamps;
    }

    public void setStamps(Integer stamps) {
        this.stamps = stamps;
    }

    public void setBanners(List<BannerTable> banners) {
        this.banners = banners;
    }

    public String getNextpage() {
        return nextpage;
    }

    public void setNextpage(String nextpage) {
        this.nextpage = nextpage;
    }

    public String getForceupdate() {
        return forceupdate;
    }

    public void setForceupdate(String forceupdate) {
        this.forceupdate = forceupdate;
    }

    public Boolean getUpdate() {
        return update;
    }

    public void setUpdate(Boolean update) {
        this.update = update;
    }

    public Integer getMinversion() {
        return minversion;
    }

    public void setMinversion(Integer minversion) {
        this.minversion = minversion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getShowdialog() {
        return showdialog;
    }

    public void setShowdialog(Boolean showdialog) {
        this.showdialog = showdialog;
    }
}
