package com.intopros.stampfee.dashboard.slider;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.intopros.stampfee.R;
import com.intopros.stampfee.database.BannerTable;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ss.com.bannerslider.Slider;
import ss.com.bannerslider.event.OnSlideClickListener;

public class ImageSliderView extends RecyclerView.ViewHolder {


    View rootView;
    @BindView(R.id.imgslider)
    Slider imgslider;

    public ImageSliderView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);

    }

    public void setItem(final List<BannerTable> mbanner) {
        imgslider.setAdapter(new MainSliderAdapter(mbanner));
        if (mbanner.size() > 0) {
            imgslider.setOnSlideClickListener(new OnSlideClickListener() {
                @Override
                public void onSlideClick(int position) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse(mbanner.get(position).getUrl().trim()));
                        rootView.getContext().startActivity(browserIntent);
                    } catch (Exception e) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse(mbanner.get(0).getUrl().trim()));
                        rootView.getContext().startActivity(browserIntent);
                    }
                }
            });
        }
    }

}
