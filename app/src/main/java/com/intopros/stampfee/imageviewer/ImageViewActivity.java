package com.intopros.stampfee.imageviewer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.utils.ZoomableImageView;

public class ImageViewActivity extends AbstractActivity {

    ZoomableImageView image;
    ImageView go_back;
//    ProgressBar progressBar;

    @Override
    public int setLayout() {
        return R.layout.activity_image_view;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }


    @Override
    public void onActivityCreated() {
        setPageTitle("", true);
        image = (ZoomableImageView) findViewById(R.id.image);
        go_back = (ImageView) findViewById(R.id.take_me_back);
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        String image_path = getIntent().getStringExtra("image");
        Glide.with(this).asBitmap()
                .load(image_path)
                .thumbnail(0.6f)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        image.setImageBitmap(resource);
                    }
                });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void endActivity() {
        finish();
    }
}
