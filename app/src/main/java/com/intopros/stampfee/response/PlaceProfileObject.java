package com.intopros.stampfee.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.intopros.stampfee.database.StampTable;

import java.util.List;

public class PlaceProfileObject {
    @Expose
    @SerializedName("offers")
    private List<OffersObject> offers;
    @SerializedName("redeem")
    @Expose
    private List<StampTable> redeem;

    public List<OffersObject> getOffers() {
        return offers;
    }

    public void setOffers(List<OffersObject> offers) {
        this.offers = offers;
    }

    public List<StampTable> getRedeem() {
        return redeem;
    }

    public void setRedeem(List<StampTable> redeem) {
        this.redeem = redeem;
    }
}
