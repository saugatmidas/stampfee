package com.intopros.stampfee.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StampTable {

    @SerializedName("currentstamps")
    @Expose
    private Integer currentstamps;
    @SerializedName("pending")
    @Expose
    private Integer pending;
    @SerializedName("redeemed")
    @Expose
    private String redeemed;
    @SerializedName("progesstotal")
    @Expose
    private Integer progesstotal;
    @SerializedName("progress")
    @Expose
    private Integer progress;
    @SerializedName("restaurantname")
    @Expose
    private String restaurantname;
    @SerializedName("restaurantid")
    @Expose
    private Integer restaurantid;
    @SerializedName("stamptype")
    @Expose
    private String stamptype;
    @SerializedName("stampcount")
    @Expose
    private Integer stampcount;
    @SerializedName("lastredeemedon")
    @Expose
    private String lastredeemedon;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("offertitle")
    @Expose
    private String offertitle;
    @SerializedName("bill")
    @Expose
    private Boolean bill;

    public Boolean getBill() {
        return bill;
    }

    public void setBill(Boolean bill) {
        this.bill = bill;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCurrentstamps() {
        return currentstamps;
    }

    public void setCurrentstamps(Integer currentstamps) {
        this.currentstamps = currentstamps;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    public String getRedeemed() {
        return redeemed;
    }

    public void setRedeemed(String redeemed) {
        this.redeemed = redeemed;
    }

    public Integer getProgesstotal() {
        return progesstotal;
    }

    public void setProgesstotal(Integer progesstotal) {
        this.progesstotal = progesstotal;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getRestaurantname() {
        return restaurantname;
    }

    public void setRestaurantname(String restaurantname) {
        this.restaurantname = restaurantname;
    }

    public Integer getRestaurantid() {
        return restaurantid;
    }

    public void setRestaurantid(Integer restaurantid) {
        this.restaurantid = restaurantid;
    }

    public String getStamptype() {
        return stamptype;
    }

    public void setStamptype(String stamptype) {
        this.stamptype = stamptype;
    }

    public Integer getStampcount() {
        return stampcount;
    }

    public void setStampcount(Integer stampcount) {
        this.stampcount = stampcount;
    }

    public String getLastredeemedon() {
        return lastredeemedon;
    }

    public void setLastredeemedon(String lastredeemedon) {
        this.lastredeemedon = lastredeemedon;
    }

    public String getOffertitle() {
        return offertitle;
    }

    public void setOffertitle(String offertitle) {
        this.offertitle = offertitle;
    }
}