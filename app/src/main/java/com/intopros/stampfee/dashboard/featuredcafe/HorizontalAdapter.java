package com.intopros.stampfee.dashboard.featuredcafe;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.newplaceprofile.NewPlaceProfileActivity;
import com.intopros.stampfee.placeprofile.PlaceProfileActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HorizontalAdapter extends AbstractAdapter<RestroTable> {

    public HorizontalAdapter(Activity activity, List<RestroTable> list) {
        this.activity = activity;
        this.mItemList = list;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feature, parent, false);
        return new CafeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder viewHolder, final int position) {
        CafeHolder holder = (CafeHolder) viewHolder;
        holder.tv_cafe_name.setText(mItemList.get(position).getRestaurantname());
        holder.tv_cafe_location.setText(mItemList.get(position).getAddress());
        if(mItemList.get(position).getImages()!=null)
        holder.setImage(mItemList.get(position).getImages().split(",")[0]);

        holder.rView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, NewPlaceProfileActivity.class).putExtra("restro", ServiceGenerator.getGson().toJson(mItemList.get(position))));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}