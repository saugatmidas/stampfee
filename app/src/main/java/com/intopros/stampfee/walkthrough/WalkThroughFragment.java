package com.intopros.stampfee.walkthrough;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractFragment;
import com.intopros.stampfee.base.Goto;

import butterknife.BindView;
import butterknife.OnClick;

public class WalkThroughFragment extends AbstractFragment {

    @BindView(R.id.iv_content)
    ImageView iv_content;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.btn_continue)
    Button btn_continue;

    @Override
    public Activity getActivityContext() {
        return getActivity();
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_walk_through;
    }

    @Override
    public void activityCreated() {
        int position = getArguments().getInt("position");
        switch (position) {
            case 0:
                iv_content.setImageDrawable(ContextCompat.getDrawable(getActivityContext(),R.drawable.intro1));
                tv_title.setText("Welcome to Stampfee!");
                tv_description.setText("Stampfee makes it easy to collect loyalty cards from places you visit without the hassle of paper cards. Using stampfee is super easy.\n\nLet's get ready to collect stamps.");
                btn_continue.setVisibility(View.GONE);
                break;
            case 1:
                iv_content.setImageDrawable(ContextCompat.getDrawable(getActivityContext(),R.drawable.intro2));
                tv_title.setText("How to collect stamp?");
                tv_description.setText("It's so easy, all you need to do is open the app, click the big orange button at the bottom & scan the code and you are done. Your stamp is added.\n\n Your first visit to that place?\nDon't worry we will create a card for you.");
                btn_continue.setVisibility(View.GONE);
                break;
            case 2:
                iv_content.setImageDrawable(ContextCompat.getDrawable(getActivityContext(),R.drawable.intro3));
                tv_title.setText("How to find & claim stamp?");
                tv_description.setText("When a stamp is full it will be automatically added to your reward tab. All you have to do is open it & scan it at the store to claim your reward.");
                btn_continue.setVisibility(View.GONE);
                break;
            case 3:
                iv_content.setImageDrawable(ContextCompat.getDrawable(getActivityContext(),R.drawable.intro4));
                tv_title.setText("How to find stores?");
                tv_description.setText("You can search for store by name and location to find your favourite outlet to collect your rewards.");
                btn_continue.setVisibility(View.GONE);
                break;
            case 4:
                iv_content.setImageDrawable(ContextCompat.getDrawable(getActivityContext(),R.drawable.intro1));
                tv_title.setText("Ready to get stamped?");
                tv_description.setText("Visit any of our stampfeed outlets, collect your stamps and get rewards.");
                btn_continue.setVisibility(View.VISIBLE);
                break;
        }
    }

    @OnClick(R.id.btn_continue)
    public void onContinue() {

        if(getActivity().getIntent().getStringExtra("goto")==null)
        new Goto().home(getActivityContext());

        getActivityContext().finish();
    }
}
