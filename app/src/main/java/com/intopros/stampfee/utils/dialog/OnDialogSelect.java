package com.intopros.stampfee.utils.dialog;

public interface OnDialogSelect {
    //1 for success 2 for fail
    void onSuccess();
    void onCancel();
}
