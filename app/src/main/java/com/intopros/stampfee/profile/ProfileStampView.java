package com.intopros.stampfee.profile;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.intopros.stampfee.R;
import com.intopros.stampfee.database.StampTable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileStampView extends RecyclerView.ViewHolder {

    View rView;

    @BindView(R.id.tv_offer_title)
    TextView tvOfferTitle;
    @BindView(R.id.tv_place)
    TextView tvPlace;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R.id.tv_progress_message)
    TextView tvProgressMessage;

    @BindView(R.id.view_redeem)
    FrameLayout viewRedeem;
    @BindView(R.id.tv_badge)
    TextView tvBadge;

    StampTable item;

    public ProfileStampView(View view) {
        super(view);
        this.rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setItem(StampTable item) {
        this.item = item;
        tvOfferTitle.setText(item.getOffertitle());
        tvPlace.setText(item.getRestaurantname());
        tvAddress.setText(item.getAddress());
        setProgress();
        setRedeem();
    }

    private void setRedeem() {
        if (item.getPending() > 0) {
            viewRedeem.setVisibility(View.VISIBLE);
            tvBadge.setText(String.valueOf(item.getPending()));
        } else {
            viewRedeem.setVisibility(View.GONE);
        }
    }

    private void setProgress() {
        pbProgress.setMax(item.getStampcount());
        pbProgress.setProgress(item.getCurrentstamps());
        String message = item.getCurrentstamps() +
                " out of " +
                item.getStampcount();
        tvProgressMessage.setText(message);
    }
}
