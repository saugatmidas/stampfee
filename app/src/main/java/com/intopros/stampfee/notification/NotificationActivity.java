package com.intopros.stampfee.notification;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.JsonObject;
import com.gturedi.views.StatefulLayout;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NotificationActivity extends AbstractActivity {


    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.swipe_reload)
    SwipyRefreshLayout swipe_reload;

    @BindView(R.id.stateful)
    StatefulLayout stateful;

    NotificationAdapter mAdapter;
    List<NotificationObject> mList;

    String nextPage = "0";

    @Override
    public int setLayout() {
        return R.layout.activity_notification;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Notification", true);
        mList = new ArrayList<>();
        recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        mAdapter = new NotificationAdapter(getActivityContext(), mList);
        recycler_view.setAdapter(mAdapter);

        swipe_reload.post(new Runnable() {
            @Override
            public void run() {
                requestNotification();
            }
        });
        swipe_reload.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    nextPage = "0";
                    requestNotification();
                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                    onScrolledToBottom();
                }
            }
        });
    }

    public void onScrolledToBottom() {
        if (!nextPage.equals("0")) {
            requestNotification();
        } else {
            swipe_reload.setRefreshing(false);
        }
    }

    private void requestNotification() {
        swipe_reload.setRefreshing(true);
        stateful.showContent();
        new SetRequest().get(getActivityContext()).set(ServiceGenerator.createService()
                .getNotifications(nextPage)).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                if (nextPage.equals("0"))
                    mList.clear();
                swipe_reload.setRefreshing(false);
                ResponseClass.NotificationObj response = ServiceGenerator.getGson().fromJson(res, ResponseClass.NotificationObj.class);
                mList.addAll(response.getResponse());
                nextPage = response.getExtraparams().getNextpage();
                mAdapter.notifyDataSetChanged();
                if (mList.isEmpty()) {
                    stateful.showEmpty();
                }
            }

            @Override
            public void OnError(String err, String errcode, int code, JsonObject res) {

                swipe_reload.setRefreshing(false);
                if (mList.isEmpty()) {
                    if (code == 204) {
                        stateful.showEmpty("No Notification Available");
                    } else {
                        stateful.showError(err, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                swipe_reload.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        requestNotification();
                                    }
                                });

                            }
                        });
                    }
                }
            }
        });
    }

}
