package com.intopros.stampfee.utils.dialog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DialogOption {
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getValue() {
        return Integer.parseInt(value);
    }

    public void setValue(String value) {
        this.value = value;
    }
}
