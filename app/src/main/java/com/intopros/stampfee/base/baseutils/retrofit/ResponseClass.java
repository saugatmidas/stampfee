package com.intopros.stampfee.base.baseutils.retrofit;

import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.database.UserInfoTable;
import com.intopros.stampfee.notification.NotificationObject;
import com.intopros.stampfee.response.DashboardObject;
import com.intopros.stampfee.response.OffersObject;
import com.intopros.stampfee.response.PlaceProfileObject;
import com.intopros.stampfee.scan.ScanObject;
import com.intopros.stampfee.utils.dialog.DialogResponse;

public class ResponseClass {


    public class PlaceProfileResponse extends ResponseObject<PlaceProfileObject> {
    }

    public class StringResponse extends ResponseObject<String> {
    }

    public class UserResponse extends ResponseObject<UserInfoTable> {
    }

    public class OffersResponse extends ResponseArray<OffersObject> {
    }

    public class RestroResponse extends ResponseArray<RestroTable> {
    }

    public class DashboardResponse extends ResponseObject<DashboardObject> {
    }

    public class ProfileResponse extends ResponseArray<StampTable> {
    }

    public class DialogObj extends ResponseArray<DialogResponse> {
    }

    public class NotificationObj extends ResponseArray<NotificationObject> {
    }

    public class StampResponse extends ResponseObject<ScanObject> {

    }
}
