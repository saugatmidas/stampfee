package com.intopros.stampfee;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.JsonObject;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.dashboard.HomeAdapter;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.database.DashboardTable;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.profile.ProfileActivity;
import com.intopros.stampfee.qrreader.BarcodeReaderActivity;
import com.intopros.stampfee.scan.ScanCountActivity;
import com.intopros.stampfee.scan.ServiceDialog;
import com.intopros.stampfee.searchplaces.SearchPlacesActivity;
import com.intopros.stampfee.utils.NavDrawerRow;
import com.intopros.stampfee.utils.PermissionUtils;
import com.intopros.stampfee.utils.Utilities;
import com.intopros.stampfee.utils.dialog.DialogHandler;
import com.intopros.stampfee.utils.dialog.EditTextDialog;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;
import com.intopros.stampfee.utils.location.CheckLocationPermission;
import com.intopros.stampfee.utils.location.GoogleFusedLocation;
import com.intopros.stampfee.walkthrough.WalkThroughActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class HomeActivity extends AbstractActivity {

    private static final int REQUEST_FINE_LOCATION_PERMISSION = 30;

    @BindView(R.id.homelist)
    RecyclerView homelist;

    @BindView(R.id.errorblock)
    RelativeLayout errorblock;
    @BindView(R.id.errmsg)
    TextView errmsg;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.nav_view)
    NavigationView nav_view;

    AlertDialog p;

    NavDrawerRow menu_home,
            menu_profile,
            menu_share_app,
            menu_help_app,
            menu_contact,
            menu_rate_app,
            menu_feedback;

    ArrayList<HomeModel> mlist = new ArrayList<>();
    HomeAdapter adapter;
    GridLayoutManager layoutManager;

    SyncDatabase syncdb;
    List<DashboardTable> blocks;

    @Override
    public int setLayout() {
        return R.layout.activity_home_container;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Stampfee", false);
        syncdb = new SyncDatabase();
        if (getBoolPref(SharedValue.showDialog))
            requestForDialog();
        setupNavigationView();
        layoutManager = new GridLayoutManager(getActivityContext(), 4);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case 7:
                        return 1;
                    case 2:
                        return 1;
                    default:
                        return 4;
                }
            }
        });

        homelist.setLayoutManager(layoutManager);

        OverScrollDecoratorHelper.setUpOverScroll(homelist, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        adapter = new HomeAdapter(this, mlist);
        homelist.setAdapter(adapter);
        loadData();
        requestData();
    }

    private void setupNavigationView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer_layout, null,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();
        menu_feedback = findViewById(R.id.menu_feedback);
        menu_home = findViewById(R.id.menu_home);
        menu_profile = findViewById(R.id.menu_profile);
        menu_share_app = findViewById(R.id.menu_share_app);
        menu_rate_app = findViewById(R.id.menu_rate_app);
        menu_contact = findViewById(R.id.menu_contact);
        menu_help_app = findViewById(R.id.menu_help_app);

        menu_home.setupView(R.drawable.ic_home_black, getString(R.string.menu_home), "View your dashboard");
        menu_profile.setupView(R.drawable.ic_account, getString(R.string.menu_profile), "View your details");
        menu_feedback.setupView(R.drawable.ic_feedback, getString(R.string.menu_feedback), "Give us feedback");
        menu_share_app.setupView(R.drawable.ic_share, getString(R.string.menu_share_app), "Share this word with your friends");
        menu_rate_app.setupView(R.drawable.ic_star, getString(R.string.menu_rate_app), "Like the app? Rate us on playstore");
        menu_contact.setupView(R.drawable.stampfeetrns, getString(R.string.menu_contact), "Stamp your business with us");
        menu_help_app.setupView(R.drawable.ic_help_outline_black_24dp, getString(R.string.menu_help_app), "Learn how Stampfee works");
    }

    @OnClick(R.id.iv_notification)
    public void openNavigation() {
        drawer_layout.openDrawer(Gravity.START);
    }

    @OnClick(R.id.menu_home)
    public void home() {
        drawer_layout.closeDrawer(Gravity.START);
    }

    @OnClick(R.id.menu_profile)
    public void profile() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), ProfileActivity.class));
    }

    @OnClick(R.id.search)
    public void search() {
        startActivity(new Intent(getActivityContext(), SearchPlacesActivity.class));
    }

    @OnClick(R.id.menu_help_app)
    public void help() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), WalkThroughActivity.class).putExtra("goto", "home"));

    }

    @OnClick(R.id.menu_contact)
    public void contact() {
        drawer_layout.closeDrawer(Gravity.START);

        final EditTextDialog input = new EditTextDialog(getActivityContext(), null);
        input.setTitle("Stamp your business with us");
        input.btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(input.getFeedback())) {
                    new SetRequest().get(getActivityContext()).pdialog(true).set(ServiceGenerator.createService()
                            .setDialog("-1", input.getFeedback())).start(new OnRes() {
                        @Override
                        public void OnSuccess(JsonObject res) {
                            input.dialog.dismiss();
                            Utilities.Toaster(getActivityContext(), "You will hear from us soon :)");
                        }

                        @Override
                        public void OnError(String err, String errcode, int code, JsonObject res) {
                            input.dialog.dismiss();
                            Utilities.Toaster(getActivityContext(), err);
                        }
                    });
                } else
                    Utilities.Toaster(getActivityContext(), "Please drop something to get back to you");

            }
        });
        input.setHint("Drop your business name, contact number and email address(optional) here");
        input.setOk("Send");
        input.setClose();
        input.show();
    }

    @OnClick(R.id.menu_feedback)
    public void menu_feedback() {
        drawer_layout.closeDrawer(Gravity.START);

        final EditTextDialog input = new EditTextDialog(getActivityContext(), null);
        input.setTitle("Give us Feedback");
        input.btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(input.getFeedback())) {
                    new SetRequest().get(getActivityContext()).pdialog(true).set(ServiceGenerator.createService()
                            .setDialog("0", input.getFeedback())).start(new OnRes() {
                        @Override
                        public void OnSuccess(JsonObject res) {
                            input.dialog.dismiss();
                            Utilities.Toaster(getActivityContext(), "Thank you for your feedback :)");
                        }

                        @Override
                        public void OnError(String err, String errcode, int code, JsonObject res) {
                            input.dialog.dismiss();
                            Utilities.Toaster(getActivityContext(), err);
                        }
                    });
                } else
                    Utilities.Toaster(getActivityContext(), "Please write some feedback for us");

            }
        });
        input.setHint("Please write your feedback here");
        input.setOk("Send");
        input.setClose();
        input.show();
    }

    @OnClick(R.id.menu_share_app)
    public void shareApp() {
        drawer_layout.closeDrawer(Gravity.START);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        String packageName = getActivityContext().getPackageName();
        sharingIntent.setType("text/plain");
        String shareBodyText = "Tired of carrying loyalty cards? \nCarry your stamp cards in your pocket. Download for free ... \n" + "https://play.google.com/store/apps/details?id=" + packageName;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share app");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
        startActivity(Intent.createChooser(sharingIntent, "Share to"));
    }

    @OnClick(R.id.menu_rate_app)
    public void rateApp() {
        drawer_layout.closeDrawer(Gravity.START);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String packageName = getActivityContext().getPackageName();
        intent.setData(Uri.parse("market://details?id=" + packageName));
        if (MyStartActivity(intent)) {
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
            if (MyStartActivity(intent)) {
                Toast.makeText(getActivityContext(), "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadData() {
        blocks = syncdb.getDashboard();
        mlist.clear();
        mlist.add(new HomeModel("slider"));
        mlist.add(new HomeModel("color", "Offers", "offer", R.color.g_green, R.drawable.ic_card_giftcard_black_24dp));
        mlist.add(new HomeModel("color", "My Cards", "profile", R.color.g_blue, R.drawable.ic_loyalty_black_24dp));
        mlist.add(new HomeModel("color", "Quick Scan", "scan", R.color.homerate, R.drawable.ic_qr_code));
        mlist.add(new HomeModel("color", "My Rewards", "reward", R.color.g_gold, R.drawable.ic_stars_black_24dp));
        for (int i = 0; i < blocks.size(); i++) {
            if (blocks.get(i).getDirection().toLowerCase().trim().equals("horizontal")) {
                try {
                    switch (blocks.get(i).getCategory()) {
                        case "video":
                            mlist.add(new HomeModel("video", syncdb.getVideos(), blocks.get(i).getTitle()));
                            break;
                        case "restaurant":
                            mlist.add(new HomeModel("feature", blocks.get(i).getTitle(), syncdb.getRestro(blocks.get(i).getType())));
                            break;
                    }
                } catch (Exception e) {
                    mlist.add(new HomeModel("feature", blocks.get(i).getTitle(), syncdb.getRestro(blocks.get(i).getType())));
                }
            } else if (blocks.get(i).getDirection().toLowerCase().trim().equals("vertical")) {
                mlist.add(new HomeModel("title").setTitle(blocks.get(i).getTitle()));
                for (int j = 0; j < syncdb.getRestro(blocks.get(i).getType()).size(); j++) {
                    mlist.add(new HomeModel("restrostamp", syncdb.getRestro(blocks.get(i).getType()).get(j)));
                }
            } else {
                mlist.add(new HomeModel("title").setTitle(blocks.get(i).getTitle()));
                for (int j = 0; j < syncdb.getRestro(blocks.get(i).getType()).size(); j++) {
                    mlist.add(new HomeModel("circle", syncdb.getRestro(blocks.get(i).getType()).get(j)));
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void requestData() {

        new SetRequest().get(getActivityContext()).pdialog(false).set(ServiceGenerator.createService()
                .getDashboard()).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                ResponseClass.DashboardResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.DashboardResponse.class);
                syncdb.deleteDashboardTitle();
                syncdb.deleteRestro();
                syncdb.deleteVideos();
                syncdb.saveDashboardTitle(response.getResponse().getBlocks());
                syncdb.saveRestro(response.getResponse().getRestro());
                try {
                    syncdb.saveVideos(response.getResponse().getVideo());
                } catch (Exception e) {

                }
                loadData();
            }

            @Override
            public void OnError(String err, String errcode, int code, JsonObject res) {
                errorblock.setVisibility(View.VISIBLE);
                errmsg.setText(err);
            }
        });

        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.errbtn)
    public void errClick() {
        errorblock.setVisibility(View.GONE);

        requestData();
    }

    @OnClick(R.id.qrscanner)
    public void scanCode() {
        getLocation();
    }

    public void getLocation() {
        new GoogleFusedLocation(getActivityContext(), new GoogleFusedLocation.SetLocationListener() {
            @Override
            public void onLocationReceived(Location location) {
                setPref(SharedValue.currentLat, location.getLatitude() + "");
                setPref(SharedValue.currentLong, location.getLongitude() + "");
                Utilities.Logger(location.getLatitude() + "  " + location.getLongitude() + " ");
                ActivityCompat.requestPermissions(getActivityContext(), new
                        String[]{Manifest.permission.CAMERA}, 20);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == BarcodeReaderActivity.BAR_CODE && data != null) {
            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
            scanstamp(barcode.rawValue, null);
        }
    }

    private void scanstamp(final String code, final String billno) {
        new SetRequest().get(this).pdialog(false)
                .set(ServiceGenerator.createService().stamp(billno, null, code, getPref(SharedValue.currentLong), getPref(SharedValue.currentLat), null))
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
                        ResponseClass.StampResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.StampResponse.class);
                        if (!response.getResponse().getRequirebill()
                                && !response.getResponse().getScancard()
                                && response.getResponse().getStamptypes().size() == 0) {
                            //bill and scanCard both not required
                            new SmallDialog(HomeActivity.this, response.getMessage(), new OnDialogSelect() {
                                @Override
                                public void onSuccess() {
                                    startActivity(new Intent(getActivityContext(), ProfileActivity.class));
                                }

                                @Override
                                public void onCancel() {
                                    getActivityContext().finish();
                                }
                            }).setno("CANCEL").setyes("OK").show();
                        } else if (response.getResponse().getScancard()) {
                            Intent intent = new Intent(getActivityContext(), ScanCountActivity.class);
                            intent.putExtra("code", code);
                            intent.putExtra("billno", response.getResponse().getRequirebill());
                            if (response.getResponse().getStamptypes().size() > 0)
                                intent.putExtra("scantype", ServiceGenerator.getGson().toJson(response.getResponse().getStamptypes()));
                            startActivity(intent);
                        } else if (response.getResponse().getStamptypes().size() > 0) {
                            final ServiceDialog serviceDialog = new ServiceDialog(getActivityContext());
                            serviceDialog.setCode(code);
                            serviceDialog.addSpinnerItems(response.getResponse().getStamptypes());
                            serviceDialog.showBillNumber(response.getResponse().getRequirebill());
                            serviceDialog.show();

                        } else if (response.getResponse().getRequirebill()) {
                            final EditTextDialog input = new EditTextDialog(getActivityContext(), null);
                            input.setTitle("Link reward to bill.");
                            input.btn_save.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!TextUtils.isEmpty(input.getFeedback())) {
                                        scanstamp(code, input.getFeedback());
                                        input.dialog.dismiss();
                                    } else
                                        Utilities.Toaster(getActivityContext(), "Please enter bill number.");

                                }
                            });
                            input.setHint("Please enter bill number.");
                            input.setOk("Scan");
                            input.setClose();
                            input.show();/**/
                        }
                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        new SmallDialog(HomeActivity.this, err, new OnDialogSelect() {
                            @Override
                            public void onSuccess() {
                                getActivityContext().finish();
                            }

                            @Override
                            public void onCancel() {
                                getActivityContext().finish();
                            }
                        }).hideno().setyes("OK").show();

                    }
                });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_FINE_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    Utilities.Toaster(getActivityContext(), "Permission Denied!");
                    PermissionUtils.setShouldShowStatus(this, Manifest.permission.ACCESS_FINE_LOCATION);
                }
                break;
            case 20:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /* takePicture(); */
                    Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(getActivityContext(), true, false);
                    startActivityForResult(launchIntent, BarcodeReaderActivity.BAR_CODE);
                } else {
                    Toast.makeText(getActivityContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void requestForDialog() {
        new SetRequest().get(getActivityContext()).set(ServiceGenerator.createService()
                .getDialogs()).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                ResponseClass.DialogObj response = ServiceGenerator.getGson().fromJson(res, ResponseClass.DialogObj.class);
                new DialogHandler().openDialog(getActivityContext(), response.getResponse());

            }

            @Override
            public void OnError(String err, String errcode, int code, JsonObject res) {

            }
        });
    }

    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return false;
        } catch (ActivityNotFoundException e) {
            return true;
        }
    }

    @Override
    public void endActivity() {
        new SmallDialog(this, "Are you sure you want to close App?", new OnDialogSelect() {
            @Override
            public void onSuccess() {
                finish();
            }

            @Override
            public void onCancel() {

            }
        }).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

}
