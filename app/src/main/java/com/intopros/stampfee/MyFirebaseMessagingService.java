package com.intopros.stampfee;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.intopros.stampfee.base.baseutils.Checker;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.notification.NotificationActivity;
import com.intopros.stampfee.offerlist.OfferListActivity;
import com.intopros.stampfee.profile.ProfileActivity;
import com.intopros.stampfee.update.UpdateActivity;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    final static String TAG = "FirebaseService";
    NotificationManager mNotificationManager;

    @Override
    public void onNewToken(String token) {
//        Log.d("sdfff", "=+++==" + token);

        SharedPreferencesHelper.setSharedPreferences(getApplicationContext(), SharedValue.fcmToken, token);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.d("sdfff", "=+++==" + remoteMessage.getData());
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            scheduleNotification(remoteMessage.getData());
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
        }
    }

    private void scheduleNotification(Map<String, String> notification) {
        try {
            if (new SyncDatabase().getUserData().getApiToken() != null) {
                if (!Checker.nullString(notification.get("excludeuserid")).equals(new SyncDatabase().getUserData().getUserid())) {
                    mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    Notification noti;
                    SharedPreferencesHelper.setSharedPreferences(getApplicationContext(), "minversion", Integer.parseInt(notification.get("minversion")));
                    if (notification.get("image") == null) {
                        noti = sendNotification(notification.get("title"), notification.get("body"), notification.get("type"), openActivity(notification.get("type"), notification.get("recordid"), Integer.parseInt(notification.get("minversion"))));
                    } else {
                        noti = setOfferNotification(notification.get("title"), notification.get("body"), notification.get("logo"), notification.get("image"), openActivity(notification.get("type"), notification.get("recordid"), Integer.parseInt(notification.get("minversion"))));
                    }
                    mNotificationManager.notify(17, noti);
                }
            }
        } catch (Exception e) {
//            Log.d("sdf", "sdf" + e.getMessage());
        }

    }

    private PendingIntent openActivity(String type, String id, int mver) {
        PendingIntent resultPendingIntent;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        if (mver > BuildConfig.VERSION_CODE) {
            stackBuilder.addNextIntent(new Intent(this, UpdateActivity.class).putExtra("skip", false));
        } else {
            switch (type.trim().toLowerCase()) {
                case "home":
                    stackBuilder.addNextIntent(new Intent(this, HomeActivity.class));
                    break;
                case "profile":
                    stackBuilder.addNextIntent(new Intent(this, HomeActivity.class));
                    stackBuilder.addNextIntent(new Intent(this, ProfileActivity.class));
                    break;
                case "offer":
                    stackBuilder.addNextIntent(new Intent(this, HomeActivity.class));
                    stackBuilder.addNextIntent(new Intent(this, OfferListActivity.class));
                    break;
                default:
                    stackBuilder.addNextIntent(new Intent(this, HomeActivity.class));
                    stackBuilder.addNextIntent(new Intent(this, NotificationActivity.class));
                    break;
            }
        }
        resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        return resultPendingIntent;
    }

    // [END receive_message]
    private Notification sendNotification(String title, String desc, String type, PendingIntent intent) {
        String CHANNEL_ID = "stampfee_channel";
        if (Build.VERSION.SDK_INT > 25) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, desc, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.stampfeetrns)
                .setContentTitle(title)
                .setContentText(desc)
                .setAutoCancel(true)
                .setLights(Color.YELLOW, 3000, 3000)
                .setContentIntent(intent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(desc))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        return notificationBuilder.build();
    }


    private Notification setOfferNotification(String title, String desc, String logo, String image, PendingIntent intent) {
        RemoteViews expandedView = new RemoteViews(this.getPackageName(), R.layout.notification_image);
        expandedView.setTextViewText(R.id.tv_title, title);
        expandedView.setTextViewText(R.id.tv_description, desc);
        try {
            URL url = new URL(logo);
            Bitmap images = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            expandedView.setImageViewBitmap(R.id.iv_icon, images);
        } catch (IOException e) {
            System.out.println(e);
        }
        try {
            URL url = new URL(image);
            Bitmap images = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            expandedView.setImageViewBitmap(R.id.iv_image_content, images);
        } catch (IOException e) {
            System.out.println(e);
        }

        String CHANNEL_ID = "stampfee";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT > 25) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, desc, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder notificationBuilder;
        notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        notificationBuilder.setSmallIcon(R.drawable.stampfeetrns)
                .setContentTitle(title)
                .setContentText(desc)
                .setAutoCancel(true)
                .setContentIntent(intent)
                .setLights(Color.YELLOW, 3000, 3000)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        notificationBuilder.setCustomBigContentView(expandedView);

        return notificationBuilder.build();
    }

}
