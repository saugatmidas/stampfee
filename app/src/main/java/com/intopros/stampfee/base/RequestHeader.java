package com.intopros.stampfee.base;

import android.os.Build;
import android.util.Log;

import com.intopros.stampfee.BuildConfig;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.utils.Utilities;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestHeader {

    public static Interceptor getHeader() {

        return new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request request;
                SyncDatabase syncdb = new SyncDatabase();
                String gcm = SharedPreferencesHelper.getSharedPreferences(AppController.getContext(), SharedValue.fcmToken, "");
//                Log.d("sdfff","==="+gcm);
//                String apitoken = "Bearer mimVT46dwyAKravDqj3JoOkvEBSpSIIeuaSQBaKXXbnrOvIt9cpeFct4ptfV";
                String apitoken = "";
                if (!syncdb.getApiToken().isEmpty())
                    apitoken = "Bearer " + syncdb.getApiToken();

//                Log.d("auth","==="+apitoken);
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Appversion", BuildConfig.VERSION_NAME)
                        .addHeader("Androidid", Build.ID)
                        .addHeader("Gcm", gcm)
                        .addHeader("Devicename", Build.MANUFACTURER + Build.MODEL)
                        .addHeader("Authorization", apitoken);

                Utilities.Logger("Header:");
                StringBuilder header = new StringBuilder();
                header.append("Appversion:" + BuildConfig.VERSION_NAME)
                        .append("\n")
                        .append("Androidid:" + Build.ID)
                        .append("\n")
                        .append("Gcm:" + gcm)
                        .append("\n")
                        .append("Devicename:" + Build.MANUFACTURER + Build.MODEL)
                        .append("\n")
                        .append("Authorization:" + apitoken);
                Utilities.Logger(header.toString());
                request = requestBuilder.build();

                return chain.proceed(request);
            }
        };
    }
}
