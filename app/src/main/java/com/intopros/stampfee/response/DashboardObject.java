package com.intopros.stampfee.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.intopros.stampfee.database.DashboardTable;
import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.database.VideoTable;

import java.util.List;

public class DashboardObject {


    @SerializedName("blocks")
    @Expose
    private List<DashboardTable>  blocks;

    @SerializedName("restro")
    @Expose
    private List<RestroTable> restro;

    @SerializedName("video")
    @Expose
    private List<VideoTable> video;

    public List<DashboardTable> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<DashboardTable> blocks) {
        this.blocks = blocks;
    }

    public List<RestroTable> getRestro() {
        return restro;
    }

    public void setRestro(List<RestroTable> restro) {
        this.restro = restro;
    }

    public List<VideoTable> getVideo() {
        return video;
    }

    public void setVideo(List<VideoTable> video) {
        this.video = video;
    }
}
