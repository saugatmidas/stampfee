package com.intopros.stampfee.login;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.Task;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.utils.dialog.SmallDialog;
import com.intopros.stampfee.walkthrough.WalkThroughActivity;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends AbstractActivity implements Login.View {

    final static int RC_GOOGLE_SIGN_IN = 100;
    Login.Presenter presenter;
    GoogleSignInClient mGoogleSignInClient;
    private AccessToken mAccessToken;
    private CallbackManager callbackManager;
    LoginManager loginManager;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;

    @Override
    public int setLayout() {
        return R.layout.activity_login;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Login", false);
        presenter = new LoginPresenter(getActivityContext());
        // Build a GoogleSignInClient with the options specified by gso.
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mAccessToken = loginResult.getAccessToken();
                presenter.getUserProfile(mAccessToken, etPhoneNumber.getText().toString());
            }

            @Override
            public void onCancel() {
                updateUI(false, "Facebook Login was canceled.");

            }

            @Override
            public void onError(FacebookException error) {
                updateUI(false, "Error logging in with Facebook. Please try again later");
            }
        });

    }

    @Override
    public void updateUI(Boolean success, String message) {
        if (success) {
//            new Goto().home(getActivityContext());
            startActivity(new Intent(getActivityContext(), WalkThroughActivity.class));
            finish();
        } else {
            SmallDialog smallDialog = new SmallDialog(getActivityContext(), message, null);
            smallDialog.hideno();
            smallDialog.setyes("OK");
            smallDialog.show();
        }
    }

    @OnClick(R.id.google_sign_in)
    public void initiateGoogleSignIn() {
        if (!TextUtils.isEmpty(etPhoneNumber.getText().toString()) && etPhoneNumber.getText().toString().matches("^[9].{9}$")) {
            mGoogleSignInClient = presenter.getGoogleSignUpClient();
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN);
        } else {
            etPhoneNumber.setError("Please enter valid mobile number");
            etPhoneNumber.requestFocus();
        }

    }

    @OnClick(R.id.fb_sign_in)
    public void initiateFbSignIn() {
        if (!TextUtils.isEmpty(etPhoneNumber.getText().toString()) && etPhoneNumber.getText().toString().matches("^[9].{9}$")) {
            loginManager.logInWithReadPermissions(getActivityContext(), Arrays.asList("public_profile", "email"));
        } else {
            etPhoneNumber.setError("Please enter valid mobile number");
            etPhoneNumber.requestFocus();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            presenter.handleGoogleSignInResult(task, etPhoneNumber.getText().toString());

        }
    }
}
