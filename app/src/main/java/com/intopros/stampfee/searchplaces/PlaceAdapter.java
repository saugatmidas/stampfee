package com.intopros.stampfee.searchplaces;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.newplaceprofile.NewPlaceProfileActivity;

import java.util.List;

public class PlaceAdapter extends AbstractAdapter<RestroTable> {

    public PlaceAdapter(Activity a,List<RestroTable> mlist) {
        this.mItemList=mlist;
        this.activity=a;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_place, viewGroup, false);
        return new PlaceView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,final int position) {
        final PlaceView holder = (PlaceView) viewHolder;
        holder.setName(mItemList.get(position).getRestaurantname());
        holder.setAddress(mItemList.get(position).getAddress());
        holder.hasOffer(mItemList.get(position).getHasoffer());
        holder.setDesc(mItemList.get(position).getStamps() + " Stamps");
        if(mItemList.get(position).getImages()!=null)
        holder.setImage(mItemList.get(position).getImages().split(",")[0]);
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, NewPlaceProfileActivity.class).putExtra("restro", ServiceGenerator.getGson().toJson(mItemList.get(position))));

            }
        });
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
