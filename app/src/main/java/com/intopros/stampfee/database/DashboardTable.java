package com.intopros.stampfee.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

@Entity
public class DashboardTable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("direction")
    @Expose
    private String direction;
    @Unique
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("category")
    @Expose
    private String category;

    @Generated(hash = 543399458)
    public DashboardTable(String title, String direction, Integer type,
            Integer priority, String category) {
        this.title = title;
        this.direction = direction;
        this.type = type;
        this.priority = priority;
        this.category = category;
    }

    @Generated(hash = 445204188)
    public DashboardTable() {
    }


    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirection() {
        return this.direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
