package com.intopros.stampfee.dashboard.featuredcafe;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class FeaturedView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.title)
    public TextView title;
    @BindView(R.id.viewall)
    public TextView viewall;
    @BindView(R.id.h_recycler)
    public RecyclerView h_recycler;

    public HorizontalAdapter hadapter;

    public FeaturedView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setAdapter(HorizontalAdapter adap) {
        h_recycler.setLayoutManager(new LinearLayoutManager(rView.getContext(), LinearLayout.HORIZONTAL, false));
        hadapter = adap;
        OverScrollDecoratorHelper.setUpOverScroll(h_recycler, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);

        h_recycler.setAdapter(hadapter);
//        PagerSnapHelper slider = new PagerSnapHelper();
//        slider.attachToRecyclerView(h_recycler);
    }
    public void setTitle(String  title) {
        this.title.setText(title);
    }
}
