package com.intopros.stampfee.dashboard;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class LoadingHolder extends RecyclerView.ViewHolder {

    public View rView;

    public LoadingHolder(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);

    }
}