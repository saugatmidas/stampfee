package com.intopros.stampfee.scan;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScanCountView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.numbers)
    public TextView numbers;

    public ScanCountView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);

    }

    public void setName(String txt){
        numbers.setText(txt);
    }


    public void makeSelection(boolean selected) {
        if (selected) {
            numbers.setBackgroundColor(rView.getContext().getResources().getColor(R.color.homerate));
            numbers.setTextColor(rView.getContext().getResources().getColor(R.color.colorWhite));
        } else {
            numbers.setBackgroundColor(rView.getContext().getResources().getColor(R.color.colorWhite));
            numbers.setTextColor(rView.getContext().getResources().getColor(R.color.transparent_black));
        }
    }

}