package com.intopros.stampfee.base.baseutils.retrofit;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiService {

    @GET(URLs.home)
    Call<JsonObject> getDashboard();

    @FormUrlEncoded
    @POST(URLs.profile)
    Call<JsonObject> getprofile(@Field("source") String source);

    @GET(URLs.offers)
    Call<JsonObject> offers();

    @GET(URLs.checkupdate)
    Call<JsonObject> checkUpdate();

    @FormUrlEncoded
    @POST(URLs.stamp)
    Call<JsonObject> stamp(@Field("billno") String billno,@Field("stampcount") String stampcount, @Field("restrocode") String restrocode, @Field("longitude") String longitude, @Field("latitude") String latitude, @Field("stamptype") String stamptype);

    @FormUrlEncoded
    @POST(URLs.scan)
    Call<JsonObject> scan(@Field("stampcount") int stampcount, @Field("restrocode") String restrocode, @Field("longitude") String longitude, @Field("latitude") String latitude);


    @FormUrlEncoded
    @POST(URLs.redeem)
    Call<JsonObject> redeem(@Field("stamptype") String stamptype,@Field("billno") String billno,@Field("restrocode") String restrocode, @Field("longitude") String longitude, @Field("latitude") String latitude);

    @FormUrlEncoded
    @PUT(URLs.register)
    Call<JsonObject> registerUser(@Field("source") String source,
                                  @Field("name") String name,
                                  @Field("email") String email,
                                  @Field("password") String password,
                                  @Field("mobileno") String mobileno,
                                  @Field("facebookid") String facebookid,
                                  @Field("photourl") String photourl,
                                  @Field("idtoken") String idtoken);

    @FormUrlEncoded
    @POST(URLs.restroprofile)
    Call<JsonObject> restroprofile(@Field("restaurantid") long restaurantid);

    @FormUrlEncoded
    @POST(URLs.restaurants)
    Call<JsonObject> restaurants(@Field("keyword") String keyword, @Field("type") String type);

    @GET(URLs.dialog)
    Call<JsonObject> getDialogs();

    @FormUrlEncoded
    @POST(URLs.dialog)
    Call<JsonObject> setDialog(@Field("dialogid") String dialogid,@Field("responsetext") String responsetext);

    @FormUrlEncoded
    @POST(URLs.notifications)
    Call<JsonObject> getNotifications(@Field("nextpage") String nextpage);

}
