package com.intopros.stampfee.scan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScanObject {

    @SerializedName("scancard")
    @Expose
    private Boolean scancard;
    @SerializedName("requirebill")
    @Expose
    private Boolean requirebill;
    @SerializedName("stamptypes")
    @Expose
    private List<StampTypeObject> stamptypes = null;

    public Boolean getScancard() {
        return scancard;
    }

    public void setScancard(Boolean scancard) {
        this.scancard = scancard;
    }

    public Boolean getRequirebill() {
        return requirebill;
    }

    public void setRequirebill(Boolean requirebill) {
        this.requirebill = requirebill;
    }

    public List<StampTypeObject> getStamptypes() {
        return stamptypes;
    }

    public void setStamptypes(List<StampTypeObject> stamptypes) {
        this.stamptypes = stamptypes;
    }
}