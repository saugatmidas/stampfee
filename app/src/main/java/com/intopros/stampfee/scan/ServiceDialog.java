package com.intopros.stampfee.scan;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.profile.ProfileActivity;
import com.intopros.stampfee.utils.Utilities;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import java.util.List;

public class ServiceDialog {

    Dialog dialog;
    TextView tv_title;
    Button btn_cancel;
    Button btn_save;

    Spinner service_spinner;
    EditText et_bill_no;

    Activity activity;
    String code;

    List<StampTypeObject> serviceList = null;
    StampTypeObject selectedService = null;

    public ServiceDialog(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity, R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_service);
        dialog.setCancelable(false);

        tv_title = dialog.findViewById(R.id.tv_title);
        service_spinner = dialog.findViewById(R.id.service_spinner);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        et_bill_no = dialog.findViewById(R.id.et_bill_no);

        et_bill_no.setVisibility(View.GONE);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scan();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public void showBillNumber(boolean showBill) {
        if (showBill)
            et_bill_no.setVisibility(View.VISIBLE);
        else
            et_bill_no.setVisibility(View.GONE);
    }

    public void addSpinnerItems(List<StampTypeObject> options) {
        serviceList = options;
        serviceList.add(0, new StampTypeObject("Select Service","-1"));
        ArrayAdapter<StampTypeObject> dataAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, serviceList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        service_spinner.setAdapter(dataAdapter);
        service_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0)
                    selectedService = serviceList.get(position);
                else
                    selectedService = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public ServiceDialog show() {
        dialog.show();
        return this;
    }

    public void scan() {
        if (et_bill_no.getVisibility() == View.VISIBLE && (et_bill_no.getText().toString().length() < 1)) {
            et_bill_no.setError("Please enter bill number.");
            Utilities.Toaster(activity, "Please enter bill number.");
        } else if (selectedService == null) {
            Utilities.Toaster(activity, "Please select service.");
        } else {
            scanstamp();
        }

    }

    private void scanstamp() {
        new SetRequest().get(activity).pdialog(false)
                .set(ServiceGenerator.createService().stamp(et_bill_no.getText().toString(), "1", code,
                        getPref(SharedValue.currentLong), getPref(SharedValue.currentLat), selectedService.getStamptype()))
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
                        dialog.dismiss();
                        ResponseClass.StampResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.StampResponse.class);
                        new SmallDialog(activity, response.getMessage(), new OnDialogSelect() {
                            @Override
                            public void onSuccess() {
                                activity.startActivity(new Intent(activity, ProfileActivity.class));
                            }

                            @Override
                            public void onCancel() {
                            }
                        }).setno("CANCEL").setyes("OK").show();

                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        dialog.dismiss();
                        new SmallDialog(activity, err, new OnDialogSelect() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onCancel() {
                            }
                        }).hideno().setyes("OK").show();

                    }
                });

    }

    public void setCode(String code) {
        this.code = code;
    }

    //get String preference
    public String getPref(String key) {
        return SharedPreferencesHelper.getSharedPreferences(activity, key, "");
    }

}
