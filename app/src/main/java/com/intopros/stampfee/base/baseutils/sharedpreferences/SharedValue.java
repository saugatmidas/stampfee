package com.intopros.stampfee.base.baseutils.sharedpreferences;


//contains string values of shared preferences used in the application
public class SharedValue {

    public static String fcmToken = "fcmToken";
    public static final String showDialog = "showdialog";
    public static final String openSettingForLocation = "openSettingForLocation";
    public static final String currentLat = "currentLat";
    public static final String currentLong= "currentLong";
}

