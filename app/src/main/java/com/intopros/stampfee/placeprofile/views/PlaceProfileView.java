package com.intopros.stampfee.placeprofile.views;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceProfileView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.tv_stamp_count)
    TextView tv_stamp_count;
    @BindView(R.id.tv_redeem)
    TextView tv_redeem;
    @BindView(R.id.desc)
    TextView desc;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;

    public PlaceProfileView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);

    }

    public void setName(String txt) {
        name.setText(txt);
    }

    public void setDesc(String txt) {
        if (!TextUtils.isEmpty(txt)) {
            desc.setVisibility(View.VISIBLE);
            desc.setText(txt);
        } else {
            desc.setVisibility(View.GONE);
        }
    }

    public void setLocation(String txt) {
        location.setText(txt);
    }

    public void setstamps(String txt) {
        tv_stamp_count.setText(txt);
    }

    public void setRedeems(String txt) {
        tv_redeem.setText(txt);
    }

    public void hideStampCount(boolean isVisible) {
        if (isVisible) {
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            linearLayout.setVisibility(View.GONE);
        }

    }

}