package com.intopros.stampfee.base;

import android.content.Context;
import android.content.Intent;

import com.intopros.stampfee.HomeActivity;
import com.intopros.stampfee.SplashActivity;
import com.intopros.stampfee.login.LoginActivity;

public class Goto {

    public void home(Context c) {
        Intent intent = new Intent(c, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(intent);
    }

    public void splash(Context c) {
        Intent intent = new Intent(c, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(intent);
    }

    public void login(Context c) {
        Intent intent = new Intent(c, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(intent);
    }
}
