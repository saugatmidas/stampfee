package com.intopros.stampfee.scan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class StampTypeObject {

    @SerializedName("stamplabel")
    @Expose
    private String stamplabel;
    @SerializedName("stamptype")
    @Expose
    private String stamptype;

    public StampTypeObject(String stamplabel, String stamptype) {
        this.stamplabel = stamplabel;
        this.stamptype = stamptype;
    }

    public String getStamplabel() {
        return stamplabel;
    }

    public void setStamplabel(String stamplabel) {
        this.stamplabel = stamplabel;
    }

    public String getStamptype() {
        return stamptype;
    }

    public void setStamptype(String stamptype) {
        this.stamptype = stamptype;
    }

    @NotNull
    @Override
    public String toString() {
        return stamplabel;
    }
}