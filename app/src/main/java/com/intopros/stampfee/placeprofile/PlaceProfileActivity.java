package com.intopros.stampfee.placeprofile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.JsonObject;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.qrreader.BarcodeReaderActivity;
import com.intopros.stampfee.response.PlaceProfileObject;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class PlaceProfileActivity extends AbstractActivity {

    private final int CLAIM = 199;

    @BindView(R.id.profilelist)
    RecyclerView profilelist;

    @BindView(R.id.errorblock)
    RelativeLayout errorblock;
    @BindView(R.id.errmsg)
    TextView errmsg;

    ArrayList<HomeModel> mlist = new ArrayList<>();
    PlaceProfileAdapter adapter;
    GridLayoutManager layoutManager;

    RestroTable restro;
    SyncDatabase syncdb;

    @Override
    public int setLayout() {
        return R.layout.activity_place_profile;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Stampfee", true);
        syncdb = new SyncDatabase();
        restro = ServiceGenerator.getGson().fromJson(getIntent().getStringExtra("restro"), RestroTable.class);

        hideShadow();
        layoutManager = new GridLayoutManager(getActivityContext(), 3);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case 7:
                        return 1;
                    case 2:
                        return 1;
                    default:
                        return 3;
                }
            }
        });
        profilelist.setLayoutManager(layoutManager);

        OverScrollDecoratorHelper.setUpOverScroll(profilelist, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        adapter = new PlaceProfileAdapter(this, mlist);
        profilelist.setAdapter(adapter);

        loadData(null);
        requestData();
    }

    private void loadData(PlaceProfileObject response) {
        mlist.clear();
        mlist.add(new HomeModel("slider"));
        mlist.add(new HomeModel("profile"));

        mlist.add(new HomeModel("color", "Call", "call", restro.getPhone() != null ? R.color.g_green : R.color.icon_grey, R.drawable.ic_call_black_24dp));

        mlist.add(new HomeModel("color", "Locate", "map", restro.getLatitude() != 0 ? R.color.g_blue : R.color.icon_grey, R.drawable.ic_pin_drop_black_24dp));
        mlist.add(new HomeModel("color", "Website", "fb", restro.getWebsite() != null ? R.color.com_facebook_button_background_color : R.color.icon_grey, R.drawable.ic_web_black_24dp));
//        mlist.add(new HomeModel("color", "Quick Scan", "scan", R.color.homerate, R.drawable.ic_qr_code));
        if (response != null) {
            if (response.getRedeem().size() > 0) {
                mlist.add(new HomeModel("title").setTitle("My Reward"));
                for (int i = 0; i < response.getRedeem().size(); i++)
                    mlist.add(new HomeModel("reward", response.getRedeem().get(i)));
            }
            if (response.getOffers().size() > 0) {
                mlist.add(new HomeModel("title").setTitle("Current Offer"));
                for (int i = 0; i < response.getOffers().size(); i++)
                    mlist.add(new HomeModel("offer", response.getOffers().get(i)));
            }
        }

        adapter.notifyDataSetChanged();

    }


    private void requestData() {
        new SetRequest().get(getActivityContext()).pdialog(false).set(ServiceGenerator.createService()
                .restroprofile(restro.getRestaurantid())).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                ResponseClass.PlaceProfileResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.PlaceProfileResponse.class);
                loadData(response.getResponse());
            }

            @Override
            public void OnError(String err, String errcode, int code, JsonObject res) {
                errorblock.setVisibility(View.VISIBLE);
                errmsg.setText(err);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 24:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /* takePicture(); */
                    Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(getActivityContext(), true, false);
                    startActivityForResult(launchIntent, CLAIM);
                } else {
                    Toast.makeText(getActivityContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }
        if (requestCode == CLAIM && data != null) {
            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
            Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
            redeem(barcode.rawValue);
        }
    }


    private void redeem(String code) {
        new SetRequest().get(getActivityContext()).pdialog(false)
                .set(ServiceGenerator.createService().redeem("", "", code, getPref(SharedValue.currentLong), getPref(SharedValue.currentLat)))
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
                        new SyncDatabase().decreaseRedems();
                        ResponseClass.StringResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.StringResponse.class);
                        new SmallDialog(getActivityContext(), response.getMessage(), new OnDialogSelect() {
                            @Override
                            public void onSuccess() {
                                requestData();
                            }

                            @Override
                            public void onCancel() {
                                requestData();
                            }
                        }).hideno().setyes("OK").show();
                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        new SmallDialog(getActivityContext(), err, null).show();

                    }
                });

    }

    @OnClick(R.id.errbtn)
    public void errClick() {
        errorblock.setVisibility(View.GONE);
        requestData();
    }

}
