package com.intopros.stampfee.profile;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;

import java.util.List;

public class StampAdapter extends AbstractAdapter<Boolean> {
    Context a;
    String type;

    StampAdapter(Context a, List<Boolean> mlist, String type) {
        this.a = a;
        this.mItemList = mlist;
        this.type = type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_stamp_icon, viewGroup, false);
        return new StampIconView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final StampIconView holder = (StampIconView) viewHolder;

        switch (type.toLowerCase().trim()) {
            case "coffee":
                holder.ricon.setImageDrawable(a.getResources().getDrawable(R.drawable.coffee));
                break;
            case "beer":
                holder.ricon.setImageDrawable(a.getResources().getDrawable(R.drawable.beer));
                break;
            case "momo":
                holder.ricon.setImageDrawable(a.getResources().getDrawable(R.drawable.momo));
                break;
            case "pizza":
                holder.ricon.setImageDrawable(a.getResources().getDrawable(R.drawable.pizza));
                break;
            default:
                holder.ricon.setImageDrawable(a.getResources().getDrawable(R.drawable.ic_qr_code));
                break;
        }

        if (mItemList.get(position)) {
            holder.ricon.setColorFilter(ContextCompat.getColor(a, R.color.homerate), android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            holder.ricon.setColorFilter(ContextCompat.getColor(a, R.color.icon_grey), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
