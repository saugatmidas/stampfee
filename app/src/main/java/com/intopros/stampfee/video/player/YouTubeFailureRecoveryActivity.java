package com.intopros.stampfee.video.player;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.intopros.stampfee.R;

import butterknife.ButterKnife;

public abstract class YouTubeFailureRecoveryActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {
    public static final int PORTRAIT_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;

    private static final int RECOVERY_DIALOG_REQUEST = 1;


    //set layout file
    public abstract int setLayout();

    public abstract YouTubePlayerView setplayer();

    //return activity context
    public abstract Activity getActivityContext();

    //set task to be done after activity creation
    public abstract void onActivityCreated();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayout());
        ButterKnife.bind(getActivityContext());

        try {
            setplayer().initialize(getString(R.string.youtube_app_id), this);
        } catch (Exception e) {
            finish();
        }
        onActivityCreated();
//        player.setFullscreen(!fullscreen);
    }


    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format("Error", errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(getString(R.string.youtube_app_id), this);
        }
    }

    protected abstract YouTubePlayer.Provider getYouTubePlayerProvider();

}