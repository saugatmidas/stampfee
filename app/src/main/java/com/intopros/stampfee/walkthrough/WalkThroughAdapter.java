package com.intopros.stampfee.walkthrough;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class WalkThroughAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 5;

    WalkThroughAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        WalkThroughFragment fragment = new WalkThroughFragment();
        bundle.putInt("position", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
