package com.intopros.stampfee.notification;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.dashboard.LoadingHolder;
import com.intopros.stampfee.offerlist.OfferListActivity;
import com.intopros.stampfee.profile.ProfileActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NotificationAdapter extends AbstractAdapter<NotificationObject> {

    public final static int TEXT = 1;
    public final static int IMAGE = 2;

    public NotificationAdapter(Activity activity, List<NotificationObject> mList) {
        this.activity = activity;
        this.mItemList = mList;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case IMAGE:
                return new ImageNotificationView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification_image, viewGroup, false));
            case TEXT:
                return new NotificationView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification, viewGroup, false));
            default:
                return new LoadingHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_loading, viewGroup, false));
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (mItemList.get(position).getImage() == null) {
            return TEXT;
        } else {
            return IMAGE;
        }
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder viewHolder, final int i) {
        super.onBindViewHolder(viewHolder, i);
        //todo manage notification view according to condition
        if (viewHolder instanceof NotificationView) {
            NotificationView holder = (NotificationView) viewHolder;
            holder.setItem(mItemList.get(i));
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    open(mItemList.get(i).getType().toLowerCase().trim(), mItemList.get(i));
                }
            });
        } else if (viewHolder instanceof ImageNotificationView) {
            ImageNotificationView holder = (ImageNotificationView) viewHolder;
            holder.setItem(mItemList.get(i));
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    open(mItemList.get(i).getType().toLowerCase().trim(), mItemList.get(i));
                }
            });
        }

    }

    private void open(String type, NotificationObject notificationObject) {

//        'normal'=>'General Notification'
//        'update'=>'App Update'
//        'offer'=>'Offer'
//        'link'=>'Link'
//        'home'=>'Home'
//        'profile'=>'User Profile'
//        'restroprofile'=>'Restaurant Profile'

        switch (type) {
            case "offer":
                activity.startActivity(new Intent(activity, OfferListActivity.class));
                break;
            case "profile":
                activity.startActivity(new Intent(activity, ProfileActivity.class));
                break;

        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
