package com.intopros.stampfee.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class BannerTable {

    @Unique
    @SerializedName("bannerid")
    @Expose
    private Integer bannerid;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("expirydate")
    @Expose
    private String expirydate;
    @SerializedName("image")
    @Expose
    private String image;

    @Generated(hash = 1716967502)
    public BannerTable(Integer bannerid, String url, String expirydate,
            String image) {
        this.bannerid = bannerid;
        this.url = url;
        this.expirydate = expirydate;
        this.image = image;
    }

    @Generated(hash = 1345879935)
    public BannerTable() {
    }

    public Integer getBannerid() {
        return bannerid;
    }

    public void setBannerid(Integer bannerid) {
        this.bannerid = bannerid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
