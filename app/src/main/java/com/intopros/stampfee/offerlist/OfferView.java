package com.intopros.stampfee.offerlist;

import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.stampfee.R;
import com.intopros.stampfee.imageviewer.ImageViewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferView extends RecyclerView.ViewHolder {

    View rootView;

    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_offer_type)
    TextView tv_offer_type;
    @BindView(R.id.tv_scheme)
    TextView tv_scheme;
    @BindView(R.id.tv_sub_title)
    AppCompatTextView tv_sub_title;

    @BindView(R.id.iv_image_content)
    ImageView iv_image_content;

    public OfferView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);
    }

    public void setTitle(String txt) {
        tv_title.setText(txt);
    }

    public void setRestro(String txt) {
        tv_sub_title.setText(txt);
    }

    public void setDesc(String txt) {
        if (TextUtils.isEmpty(txt))
            tv_description.setVisibility(View.GONE);
        else
            tv_description.setText(txt);
    }

    public void setTerms(String txt) {
        tv_offer_type.setText(txt);
    }

    public void setOfferType(String txt) {
        tv_scheme.setText(txt);
    }

    public void setImage(final String url) {
        if (TextUtils.isEmpty(url)) {
            iv_image_content.setVisibility(View.GONE);
            tv_scheme.setVisibility(View.GONE);
        } else {
            Glide.with(rootView)
                    .load(url)
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder)
                            .transforms(new CenterCrop(), new RoundedCorners(16)))
                    .into(iv_image_content);

            iv_image_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    rootView.getContext().startActivity(new Intent(rootView.getContext(), ImageViewActivity.class)
                            .putExtra("image", url));
                }
            });
        }

    }
}
