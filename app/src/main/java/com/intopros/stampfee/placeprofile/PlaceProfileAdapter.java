package com.intopros.stampfee.placeprofile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.dashboard.CircleView;
import com.intopros.stampfee.dashboard.ColorView;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.dashboard.LoadingHolder;
import com.intopros.stampfee.dashboard.slider.FlatImageSliderView;
import com.intopros.stampfee.dashboard.stamps.TitleView;
import com.intopros.stampfee.database.RestroTable;
import com.intopros.stampfee.offerlist.OfferView;
import com.intopros.stampfee.placeprofile.views.PlaceProfileView;
import com.intopros.stampfee.reward.RewardView;
import com.intopros.stampfee.utils.Utilities;

import java.util.List;
import java.util.Locale;

public class PlaceProfileAdapter extends AbstractAdapter<HomeModel> {

    public final int SLIDER = 1;
    public final int OFFER = 5;
    public final int COLOR = 2;
    public final int TITLE = 6;
    public final int CIRCLE = 7;
    public final int REWARD = 3;
    public final int LOADING = 4;
    public final int PROFILE = 8;
    private boolean isStampCountVisible = true;

    RestroTable restro;

    public PlaceProfileAdapter(Activity a, List<HomeModel> mlist) {
        this.activity = a;
        this.mItemList = mlist;
    }

    public void setStampCountVisibilityForProfile(boolean isVisible) {
        isStampCountVisible = isVisible;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TITLE:
                return new TitleView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_title, parent, false));
            case LOADING:
                return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false));
            case SLIDER:
                return new FlatImageSliderView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_flat_imageslider, parent, false));
            case OFFER:
                return new OfferView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offer, parent, false));
            case COLOR:
                return new ColorView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_color, parent, false));
            case CIRCLE:
                return new CircleView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_circle, parent, false));
            case PROFILE:
                return new PlaceProfileView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_restro_profile, parent, false));
            case REWARD:
                return new RewardView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reward, parent, false));
            default:
                return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);

        restro = ServiceGenerator.getGson().fromJson(activity.getIntent().getStringExtra("restro"), RestroTable.class);
        if (viewHolder instanceof FlatImageSliderView) {
            FlatImageSliderView holder = (FlatImageSliderView) viewHolder;
            if (restro.getImages() != null)
                holder.setImages(restro.getImages().split(","));
        } else if (viewHolder instanceof PlaceProfileView) {
            PlaceProfileView holder = (PlaceProfileView) viewHolder;
            holder.setName(restro.getRestaurantname());
            holder.setLocation(restro.getAddress());
            holder.setstamps(restro.getStamps());
            holder.setDesc(restro.getDescription());
            holder.setRedeems(restro.getRedeems());
            holder.hideStampCount(isStampCountVisible);

        } else if (viewHolder instanceof RewardView) {
            RewardView holder = (RewardView) viewHolder;
            holder.setName(mItemList.get(position).getStamp().getRestaurantname());
            holder.setDesc(mItemList.get(position).getStamp().getDescription());
            holder.setaddress(mItemList.get(position).getStamp().getAddress());
            holder.setRemaining("+ " + mItemList.get(position).getStamp().getPending());

            holder.btn_claim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(activity, new
                            String[]{Manifest.permission.CAMERA}, 24);
                }
            });
        } else if (viewHolder instanceof ColorView) {
            ColorView holder = (ColorView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
            holder.setBackground(mItemList.get(position).getColor());
            holder.setIcon(mItemList.get(position).getIcon());

            holder.hideBadge();

            holder.rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (mItemList.get(position).getId().trim().toLowerCase()) {
                        case "call":
                            if (restro.getPhone() != null) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + restro.getPhone().trim()));
                                activity.startActivity(intent);
                            } else
                                Utilities.Toaster(activity, "Oops!!! Number not available");
                            break;
                        case "map":
                            if (restro.getLatitude() != 0) {
                                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f",
                                        restro.getLatitude(), restro.getLongitude());
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                                browserIntent.setData(Uri.parse(uri));
                                activity.startActivity(browserIntent);
                            } else
                                Utilities.Toaster(activity, "Oops!!! Location not available");
                            break;
                        case "fb":
                            if (restro.getWebsite() != null) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                                browserIntent.setData(Uri.parse(restro.getWebsite().trim()));
                                activity.startActivity(browserIntent);
                            } else
                                Utilities.Toaster(activity, "Oops!!! Website not available");
                            break;
                    }
                }
            });
        } else if (viewHolder instanceof TitleView) {
            TitleView holder = (TitleView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
        } else if (viewHolder instanceof OfferView) {
            OfferView holder = (OfferView) viewHolder;

            holder.setTitle(mItemList.get(position).getOffer().getTitle());
            holder.setDesc(mItemList.get(position).getOffer().getDescription());
            holder.setImage(mItemList.get(position).getOffer().getImage());
            holder.setOfferType(mItemList.get(position).getOffer().getOffertype());
            holder.setRestro(mItemList.get(position).getOffer().getRestaurantname());
            holder.setTerms(mItemList.get(position).getOffer().getTerms());
        }

    }

    @Override
    public int getItemViewType(int position) {
        switch (mItemList.get(position).getType()) {
            case "title":
                return TITLE;
            case "loading":
                return LOADING;
            case "slider":
                return SLIDER;
            case "circle":
                return CIRCLE;
            case "offer":
                return OFFER;
            case "reward":
                return REWARD;
            case "color":
                return COLOR;
            case "profile":
                return PROFILE;
            default:
                return LOADING;
        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
