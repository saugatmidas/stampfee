package com.intopros.stampfee.login;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.Task;

public interface Login {

    interface View {

        void updateUI(Boolean success, String message);
    }

    interface Presenter {

        GoogleSignInClient getGoogleSignUpClient();

        void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask, String phoneNumber);

        void getUserProfile(AccessToken accessToken, String phoneNumber);
        void registerUser(String name, String email, String password, String mobileno, Boolean isSignup);

    }

}
