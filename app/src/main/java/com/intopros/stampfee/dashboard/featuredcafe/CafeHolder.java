package com.intopros.stampfee.dashboard.featuredcafe;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CafeHolder extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.tv_cafe_name)
    TextView tv_cafe_name;

    @BindView(R.id.tv_cafe_location)
    TextView tv_cafe_location;
    @BindView(R.id.iv_cafe_image)
    ImageView iv_cafe_image;

    public CafeHolder(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);

    }

    public void setImage(String img){
        Glide.with(rView.getContext())
                .load(img)
                .into(iv_cafe_image);
    }
}