package com.intopros.stampfee.dashboard.videorecipe;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.intopros.stampfee.R;
import com.intopros.stampfee.dashboard.featuredcafe.HorizontalAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class VideoRecipeView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.title)
    public TextView title;
    @BindView(R.id.viewall)
    public TextView viewall;
    @BindView(R.id.h_recycler)
    public RecyclerView h_recycler;

    public HorizontalRecipeAdapter hadapter;

    public VideoRecipeView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setAdapter(HorizontalRecipeAdapter adap) {
        h_recycler.setLayoutManager(new LinearLayoutManager(rView.getContext(), LinearLayout.HORIZONTAL, false));
        hadapter = adap;
        OverScrollDecoratorHelper.setUpOverScroll(h_recycler, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);
        h_recycler.setAdapter(hadapter);;

    }
    public void setTitle(String  title) {
        this.title.setText(title);
    }
}
