package com.intopros.stampfee.video;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoListHolder extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.vid_image)
    ImageView thumb;
    @BindView(R.id.timmer)
    TextView timmer;
    @BindView(R.id.vid_title)
    TextView vid_title;
    @BindView(R.id.vid_desc)
    TextView vid_desc;
    @BindView(R.id.vid_date)
    TextView vid_date;


    public VideoListHolder(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);

    }

    public void setTitle(String title, String desc,String date,String time){
        timmer.setText(time);
        vid_title.setText(title);
        vid_desc.setText(desc);
        vid_date.setText(date);
    }

    public void setImage(String image) {
        Glide.with(rView.getContext()).load(image)
                .thumbnail(0.5f)
                .into(thumb);

    }
}