package com.intopros.stampfee.dashboard.slider;

import com.intopros.stampfee.R;
import com.intopros.stampfee.database.BannerTable;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class MainSliderAdapter extends SliderAdapter {
    List<BannerTable> banner = new ArrayList<>();

    public MainSliderAdapter(List<BannerTable> banner) {
        this.banner = banner;
    }

    @Override
    public int getItemCount() {
        return banner.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.bindImageSlide(banner.get(position).getImage(), R.drawable.placeholder, R.drawable.placeholder);

    }
}