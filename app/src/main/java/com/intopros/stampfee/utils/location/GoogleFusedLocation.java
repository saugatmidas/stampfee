package com.intopros.stampfee.utils.location;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.intopros.stampfee.utils.PermissionUtils;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import org.jetbrains.annotations.NotNull;

public class GoogleFusedLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient googleApiClient;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 2000; // = 5 seconds

    Activity activity;
    public static final int REQUEST_FINE_LOCATION_PERMISSION = 30;
    private LocationManager locationManager;
    ProgressDialog dialog;
    private GoogleFusedLocation.SetLocationListener listener;

    public interface SetLocationListener {
        void onLocationReceived(Location location);
    }

    private Activity getActivityContext() {
        return activity;
    }

    public GoogleFusedLocation(@NotNull Activity activity, GoogleFusedLocation.SetLocationListener listener) {
        this.activity = activity;
        this.listener = listener;
        dialog = new ProgressDialog(getActivityContext());
        dialog.setCancelable(false);
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        checkPermission();
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivityContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //permission not granted
            //show explanation?
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivityContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                //no explanation needed initial request
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (PermissionUtils.neverAskAgainSelected(getActivityContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                        displayNeverAskAgainDialog();
                    } else {
                        ActivityCompat.requestPermissions(getActivityContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION_PERMISSION);
                    }
                }
            } else {
                //show explanation
                new SmallDialog(getActivityContext(), "We need to access location for performing necessary task. Please grant location permission to continue.", new OnDialogSelect() {
                    @Override
                    public void onSuccess() {
                        ActivityCompat.requestPermissions(getActivityContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION_PERMISSION);
                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
            }
        } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //permission granted but GPS is disabled
            new SmallDialog(getActivityContext(), "Location access is needed to scan the QR code. Please enable GPS.", new OnDialogSelect() {
                @Override
                public void onSuccess() {
                    getActivityContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }

                @Override
                public void onCancel() {

                }
            }).show();
        } else {
            //permission granted and GPS is enabled
            googleFusedLocation();
        }
    }

    private void displayNeverAskAgainDialog() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivityContext());
        builder.setMessage("We need to access location for performing necessary task. Please permit the permission and try again through "
                + "Settings screen.\n\nSelect Permissions -> Enable permission");
        builder.setCancelable(false);
        builder.setPositiveButton("Permit Manually", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivityContext().getPackageName(), null);
                intent.setData(uri);
                getActivityContext().startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    public void googleFusedLocation() {
        googleApiClient = new GoogleApiClient.Builder(activity).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(activity, "You need to install Google Play Services to use the App properly", Toast.LENGTH_SHORT).show();
        if (dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(activity, "You need to install Google Play Services to use the App properly", Toast.LENGTH_SHORT).show();
        if (dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onLocationChanged(Location location) {
        // stop location updates
        if (googleApiClient != null && googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
        listener.onLocationReceived(location);
        if (dialog.isShowing())
            dialog.dismiss();
    }

    private void startLocationUpdates() {
        dialog.setTitle("Fetching location");
        dialog.setMessage("Please wait for a while, it might take few minutes ... ");
        dialog.show();
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(activity, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

}
