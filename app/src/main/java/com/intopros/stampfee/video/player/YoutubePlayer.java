package com.intopros.stampfee.video.player;

import android.app.Activity;
import android.content.res.Configuration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.gturedi.views.StatefulLayout;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AppController;
import com.intopros.stampfee.database.VideoTable;
import com.intopros.stampfee.utils.dialog.SmallDialog;
import com.intopros.stampfee.video.VideoListAdapter;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;

import butterknife.BindView;

public class YoutubePlayer extends YouTubeFailureRecoveryActivity implements YouTubePlayer.OnFullscreenListener {

    private YouTubePlayer player;
    private boolean fullscreen = false;

    @BindView(R.id.player)
    YouTubePlayerView playerView;

    @BindView(R.id.videolist)
    RecyclerView videolist;

    @BindView(R.id.swipe_reload)
    SwipyRefreshLayout swipe_reload;

    @BindView(R.id.stateful)
    StatefulLayout stateful;
    @BindView(R.id.ctitle)
    TextView ctitle;
    @BindView(R.id.credit)
    TextView credit;

    VideoListAdapter adapter;
    GridLayoutManager layoutManager;

    ArrayList<VideoTable> mlist = new ArrayList<>();
    String nextPage = "0";

    String videoid = "";
    String subid = "";

    @Override
    public int setLayout() {
        return R.layout.activity_youtube_player;
    }

    @Override
    public YouTubePlayerView setplayer() {
        return playerView;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {

        videoid = getIntent().getStringExtra("vidid");
        subid = getIntent().getStringExtra("subid");
        ctitle.setText(getIntent().getStringExtra("ctitle"));
        credit.setText("- " + getIntent().getStringExtra("credit"));
        layoutManager = new GridLayoutManager(getActivityContext(), 1);
        videolist.setLayoutManager(layoutManager);
        adapter = new VideoListAdapter(this, false, mlist);
        videolist.setAdapter(adapter);
        swipe_reload.post(new Runnable() {
            @Override
            public void run() {
                requestVideo();
            }
        });
        swipe_reload.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    requestVideo();
                }
            }
        });
    }

    private void requestVideo() {
        stateful.showContent();
        swipe_reload.setRefreshing(true);
        mlist.clear();
        mlist.addAll(AppController.getQuery(VideoTable.class).build().list());
        adapter.notifyDataSetChanged();
        swipe_reload.setRefreshing(false);
    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        this.player = player;
        player.setOnFullscreenListener(this);
        if (!wasRestored) {
            player.loadVideo(getIntent().getStringExtra("url"));
        }
    }

    public void playVideo(String url, String subid, String vidid, String ctitle, String credit) {
        if (player == null) {
            SmallDialog smallDialog = new SmallDialog(getActivityContext(), "Youtube Player not found. Please install or upgrade YouTube to latest version.", null);
            smallDialog.hideno();
            smallDialog.setyes("OK");
            smallDialog.show();
        } else {
            this.ctitle.setText(ctitle);
            this.credit.setText("- " + credit);
            videoid = vidid;
            this.subid = subid;
            requestVideo();
            player.loadVideo(url);
        }
    }


    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return playerView;
    }

    @Override
    public void onFullscreen(boolean isFullscreen) {
        player.play();
        fullscreen = isFullscreen;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (fullscreen) {
            player.setFullscreen(false);
        } else {
            finish();
        }
    }
}
