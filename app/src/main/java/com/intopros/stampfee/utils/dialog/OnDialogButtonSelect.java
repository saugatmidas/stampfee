package com.intopros.stampfee.utils.dialog;

public interface OnDialogButtonSelect {

    void onSuccess();
    void onCancel();
    void onSkip();
}
