package com.intopros.stampfee.walkthrough;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.Goto;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import butterknife.BindView;
import butterknife.OnClick;

public class WalkThroughActivity extends AbstractActivity {

    @BindView(R.id.view_pager)
    ViewPager view_pager;
    @BindView(R.id.worm_dots_indicator)
    WormDotsIndicator worm_dots_indicator;

    @BindView(R.id.btn_previous)
    Button btn_previous;
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.btn_skip)
    Button btn_skip;

    private PagerAdapter pagerAdapter;
    private int selectedPage = 0;

    @Override
    public int setLayout() {
        return R.layout.activity_walkthrough;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Get Started", getIntent().getStringExtra("goto")==null?false:true);
        // Instantiate a ViewPager and a PagerAdapter.
        pagerAdapter = new WalkThroughAdapter(getSupportFragmentManager());
        view_pager.setAdapter(pagerAdapter);
        view_pager.setPageTransformer(true, new DepthPageTransformer());
        worm_dots_indicator.setViewPager(view_pager);
        btn_previous.setVisibility(View.GONE);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                selectedPage = i;
                if (i == 4)
                    btn_next.setVisibility(View.GONE);
                else
                    btn_next.setVisibility(View.VISIBLE);
                if (i == 0)
                    btn_previous.setVisibility(View.GONE);
                else
                    btn_previous.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @OnClick(R.id.btn_skip)
    public void onSkip() {
        //todo goto homepage
        if(getIntent().getStringExtra("goto")==null)
        new Goto().home(getActivityContext());

        finish();
    }

    @OnClick(R.id.btn_previous)
    public void onPrevious() {
        if (selectedPage != 0) {
            view_pager.setCurrentItem(selectedPage - 1);
        }
    }

    @OnClick(R.id.btn_next)
    public void onNext() {
        if (selectedPage != 4) {
            view_pager.setCurrentItem(selectedPage + 1);
        }
    }

}
