package com.intopros.stampfee.scan;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;

public class ScanCountAdapter extends AbstractAdapter<String> {

    int pos = 0;
    int row_index = -1;

    public ScanCountAdapter(Activity a) {
        this.activity = a;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_numbers, viewGroup, false);
        return new ScanCountView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final ScanCountView holder = (ScanCountView) viewHolder;
        holder.setName("" + (position + 1));
        holder.rView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index = holder.getAdapterPosition();
                notifyDataSetChanged();
//                ActivityCompat.requestPermissions(activity, new
//                        String[]{Manifest.permission.CAMERA}, 20);
                pos=position;
            }
        });

        if (row_index == holder.getAdapterPosition()) {
            holder.makeSelection(true);
        } else {
            holder.makeSelection(false);
        }
    }

    public int getPos() {
        return pos;
    }

    @Override
    public int getItemCount() {
        return 9;
    }
}
