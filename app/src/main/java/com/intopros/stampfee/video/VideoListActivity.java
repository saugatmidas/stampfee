package com.intopros.stampfee.video;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gturedi.views.StatefulLayout;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.AppController;
import com.intopros.stampfee.database.VideoTable;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;

import butterknife.BindView;

public class VideoListActivity extends AbstractActivity {

    @BindView(R.id.videolist)
    RecyclerView videolist;

    @BindView(R.id.swipe_reload)
    SwipyRefreshLayout swipe_reload;

    @BindView(R.id.stateful)
    StatefulLayout stateful;

    VideoListAdapter adapter;
    GridLayoutManager layoutManager;
    ArrayList<VideoTable> mlist = new ArrayList<>();

    @Override
    public int setLayout() {
        return R.layout.activity_video_list;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Videos", true);
        layoutManager = new GridLayoutManager(getActivityContext(), 1);
        videolist.setLayoutManager(layoutManager);
        adapter = new VideoListAdapter(this, true, mlist);
        videolist.setAdapter(adapter);
        swipe_reload.post(new Runnable() {
            @Override
            public void run() {
                requestVideo();
            }
        });
        swipe_reload.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    requestVideo();
                }
            }
        });
    }

    private void requestVideo() {
        stateful.showContent();
        swipe_reload.setRefreshing(true);
        mlist.clear();
        mlist.addAll(AppController.getQuery(VideoTable.class).build().list());
        adapter.notifyDataSetChanged();
        swipe_reload.setRefreshing(false);
    }

}
