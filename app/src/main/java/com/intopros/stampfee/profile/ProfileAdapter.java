package com.intopros.stampfee.profile;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.dashboard.LoadingHolder;
import com.intopros.stampfee.dashboard.featuredcafe.FeaturedView;
import com.intopros.stampfee.dashboard.stamps.TitleView;
import com.intopros.stampfee.reward.RewardActivity;

import java.util.List;

public class ProfileAdapter extends AbstractAdapter<HomeModel> {


    public final int PROFILE = 1;
    public final int TITLE = 6;
    public final int STAMP = 2;
    public final int LOADING = 4;
    public final int FEATURE = 5;


    public ProfileAdapter(Activity a, List<HomeModel> mlist) {
        this.activity = a;
        this.mItemList = mlist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TITLE:
                return new TitleView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_title, parent, false));
            case LOADING:
                return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false));
            case PROFILE:
                return new ProfileView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_profile, parent, false));
            case FEATURE:
                return new FeaturedView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recyclerview, parent, false));
            case STAMP:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_profile_stamp, parent, false);
                return new ProfileStampView(view);
            default:
                return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);

        if (viewHolder instanceof TitleView) {
            TitleView holder = (TitleView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
        } else if (viewHolder instanceof ProfileView) {
            ProfileView holder = (ProfileView) viewHolder;
            holder.setProfile();
        } else if (viewHolder instanceof StampDetailView) {
            StampDetailView holder = (StampDetailView) viewHolder;
            holder.setUpStampts(mItemList.get(position).getStamp().getStamptype());
            holder.setName(mItemList.get(position).getStamp().getRestaurantname());
            holder.setTotal(mItemList.get(position).getStamp().getStampcount()==null?0:mItemList.get(position).getStamp().getStampcount(), mItemList.get(position).getStamp().getCurrentstamps()==null?0:mItemList.get(position).getStamp().getCurrentstamps());
            holder.setDesc(mItemList.get(position).getStamp().getDescription());
            holder.setAddress(mItemList.get(position).getStamp().getAddress());
            holder.isRedeemAvailable(mItemList.get(position).getStamp().getPending() > 0);
            holder.rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    activity.startActivityForResult(new Intent(activity, StampDetailDialog.class)
                                    .putExtra("stamp", ServiceGenerator.getGson().toJson(mItemList.get(position).getStamp()))
                            , 9841);

                   /* if (mItemList.get(position).getStamp().getPending() > 0) {
                        activity.startActivity(new Intent(activity, RewardActivity.class));

                    }*/
                }
            });

        } else if (viewHolder instanceof ProfileStampView) {
            ProfileStampView holder = (ProfileStampView) viewHolder;
            holder.setItem(mItemList.get(position).getStamp());
            holder.rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    activity.startActivityForResult(new Intent(activity, StampDetailDialog.class)
                                    .putExtra("stamp", ServiceGenerator.getGson().toJson(mItemList.get(position).getStamp()))
                            , StampDetailDialog.STAMP_DIALOG);

                }
            });

        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (mItemList.get(position).getType()) {
            case "title":
                return TITLE;
            case "loading":
                return LOADING;
            case "profile":
                return PROFILE;
            case "stamp":
                return STAMP;
            case "feature":
                return FEATURE;
            default:
                return LOADING;
        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
