package com.intopros.stampfee.searchplaces;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SearchView;

import com.google.gson.JsonObject;
import com.gturedi.views.StatefulLayout;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.database.RestroTable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SearchPlacesActivity extends AbstractActivity {

    @BindView(R.id.stateful)
    StatefulLayout stateful;
    @BindView(R.id.swipe_reload)
    SwipeRefreshLayout swipe_reload;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.searchtxt)
    SearchView searchtxt;

    List<RestroTable> mlist = new ArrayList<>();
    PlaceAdapter mAdapter;
    String keyword;

    SetRequest req;


    @Override
    public int setLayout() {
        return R.layout.activity_search_places;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getIntent().getStringExtra("title") != null ? getIntent().getStringExtra("title") : "Search", true);
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        mAdapter = new PlaceAdapter(this, mlist);
        recycler_view.setAdapter(mAdapter);


        searchtxt.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                keyword = s;
                request();
                return false;
            }
        });

        swipe_reload.post(new Runnable() {
            @Override
            public void run() {
                request();
            }
        });
        swipe_reload.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                request();
            }
        });

    }

    private void request() {
        swipe_reload.setRefreshing(true);

        if (req != null) {
            req.cancel();
        }
        req = new SetRequest().get(getActivityContext()).set(ServiceGenerator.createService()
                .restaurants(keyword, getIntent().getStringExtra("type"))).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                ResponseClass.RestroResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.RestroResponse.class);
                swipe_reload.setRefreshing(false);
                mlist.clear();
                mlist.addAll(response.getResponse());
                mAdapter.notifyDataSetChanged();
                stateful.showContent();
            }

            @Override
            public void OnError(String err, String errcode, int code, JsonObject res) {
                swipe_reload.setRefreshing(false);
                if (mlist.isEmpty()) {
                    stateful.showError(err, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            swipe_reload.post(new Runnable() {
                                @Override
                                public void run() {
                                    request();
                                }
                            });
                        }
                    });
                }
            }
        });
    }


}
