package com.intopros.stampfee.login;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonObject;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.database.SyncDatabase;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginPresenter implements Login.Presenter {

    Activity activity;
    Login.View view;
    private String signUpType = "normal", googleTokenID = "", facebookID = "", photoURL = "";

    public LoginPresenter(Activity activity) {
        this.activity = activity;
        view = (Login.View) activity;
    }

    @Override
    public GoogleSignInClient getGoogleSignUpClient() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.server_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
//        if (account != null)
//            view.updateUI(true);
        return mGoogleSignInClient;
    }

    @Override
        public void handleGoogleSignInResult (Task < GoogleSignInAccount > completedTask, String phoneNumber){
            try {
                GoogleSignInAccount account = completedTask.getResult(ApiException.class);
                // Signed in successfully, show authenticated UI.
                googleTokenID = account.getIdToken();
                // get profile picture if possible
                try {
                    photoURL = GoogleSignIn.getLastSignedInAccount(activity).getPhotoUrl().toString();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                signUpType = "google";
                registerUser("", "", "", phoneNumber, true);
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w(activity.getLocalClassName(), "signInResult:failed code=" + e.getStatusCode());
                view.updateUI(false, "Cannot login with google account right now. Please try again later");
            }
        }

        @Override
        public void getUserProfile ( final AccessToken currentAccessToken, final String phoneNumber)
        {

            Bundle params = new Bundle();
            params.putBoolean("redirect", false);
            params.putString("fields", "id,name,email,picture.type(large)");
            GraphRequest request = GraphRequest.newMeRequest(currentAccessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            if (response != null) {
                                Log.v("LoginActivity", object.toString());
                                JSONObject data = response.getJSONObject();
                                if (data.has("picture")) {
                                    String name = "";
                                    String email = "";
                                    signUpType = "facebook";
                                    facebookID = currentAccessToken.getUserId();
                                    try {
                                        email = data.getString("email");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        photoURL = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        name = data.getString("name");
                                        registerUser(name, email, "", phoneNumber, true);
                                    } catch (JSONException e) {
                                        view.updateUI(false, "Cannot login with facebook right now. Please Try again later");
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                view.updateUI(false, "Cannot login with facebook right now. Please Try again later");
                            }
                        }
                    });
            request.setParameters(params);
            request.executeAsync();
        }

        @Override
        public void registerUser (String name, String email, String password, String
        mobileno, Boolean isSignUp){
            //TODO send request to server to save user data
            if (isSignUp) {
            /*if (!mobileno.isEmpty()) {
                googleTokenID = "";
                facebookID = "";
                photoURL = "";
            }*/
                new SetRequest().get(activity).pdialog("Signing in", false).set(ServiceGenerator.createService().registerUser(
                        signUpType, name, email, password, mobileno, facebookID, photoURL, googleTokenID))
                        .start(new OnRes() {
                            @Override
                            public void OnSuccess(JsonObject res) {
                                ResponseClass.UserResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.UserResponse.class);

                                new SyncDatabase().saveUserData(response.getResponse());
                                new SyncDatabase().savefullStampCount(response.getResponse().getUserid(), response.getExtraparams().getStamps(), response.getExtraparams().getReedems(), response.getExtraparams().getRedeemavailable(), response.getExtraparams().getOffersavailable());
                                new SyncDatabase().saveBanner(response.getExtraparams().getBanners());

                                view.updateUI(true, "");
                            }

                            @Override
                            public void OnError(String err, String errcode, int code, JsonObject res) {
                                view.updateUI(false, err);
                            }
                        });
            }
        }

    }
