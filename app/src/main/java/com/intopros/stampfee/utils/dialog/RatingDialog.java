package com.intopros.stampfee.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.intopros.stampfee.R;

public class RatingDialog {

    Dialog dialog;
    TextView tv_title;
    RatingBar rb_review;
    Button btn_cancel;
    Button btn_save;
    Button btn_skip;

    Activity activity;
    OnDialogButtonSelect listener;

    public RatingDialog(Activity activity, OnDialogButtonSelect buttonListener) {
        this.activity = activity;
        listener = buttonListener;
        dialog = new Dialog(activity,R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_rating);

        tv_title = dialog.findViewById(R.id.tv_title);
        rb_review = dialog.findViewById(R.id.rb_review);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        btn_skip = dialog.findViewById(R.id.btn_skip);

        btn_skip.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_save.setVisibility(View.GONE);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSuccess();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onCancel();
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSkip();
            }
        });
    }

    public float getRating() {
        return rb_review.getRating();
    }

    public RatingDialog setTitle(String title) {
        tv_title.setText(title);
        return this;
    }

    public RatingDialog setOk(String positiveText) {
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setText(positiveText);
        return this;
    }

    public RatingDialog setClose() {
        btn_cancel.setVisibility(View.VISIBLE);
        btn_cancel.setText("Close");
        return this;
    }

    public RatingDialog show() {
        dialog.show();
        return this;
    }
}
