package com.intopros.stampfee.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.intopros.stampfee.R;

import java.util.List;

public class RadioDialog {

    Dialog dialog;
    TextView tv_title;
    RadioGroup rg_radiogroup;
    Button btn_cancel;
    Button btn_save;
    Button btn_skip;

    Activity activity;
    OnDialogButtonSelect listener;

    public RadioDialog(Activity activity, OnDialogButtonSelect buttonListener) {
        this.listener = buttonListener;
        this.activity = activity;
        dialog = new Dialog(activity, R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_radio);
        dialog.setCancelable(false);

        tv_title = dialog.findViewById(R.id.tv_title);
        rg_radiogroup = dialog.findViewById(R.id.rg_radiogroup);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        btn_skip = dialog.findViewById(R.id.btn_skip);

        btn_skip.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_save.setVisibility(View.GONE);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSuccess();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onCancel();
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSkip();
            }
        });
    }

    public RadioDialog setTitle(String title) {
        tv_title.setText(title);
        return this;
    }

    public void addRadioButtons(List<DialogOption> options) {
        for (int row = 0; row < 1; row++) {
            RadioGroup radioGroup = new RadioGroup(activity);
            radioGroup.setOrientation(LinearLayout.VERTICAL);

            for (int i = 1; i <= options.size(); i++) {
                RadioButton radioButton = new RadioButton(activity);
                radioButton.setId(options.get(i - 1).getValue());
                radioButton.setText(options.get(i - 1).getLabel());
                radioGroup.addView(radioButton);
            }
            ((ViewGroup) dialog.findViewById(R.id.rg_radiogroup)).addView(radioGroup);
        }
    }

    public String getSelectedRadioValue(List<DialogOption> radioList) {
        String id = null;
        RadioGroup radioG = (RadioGroup) (((ViewGroup) dialog.findViewById(R.id.rg_radiogroup)).getChildAt(0));
        for (int i = 0; i < radioList.size(); i++) {
            if (((RadioButton) (radioG.getChildAt(i))).isChecked()) {
                id = radioList.get(i).getValue() + "";
            }
        }
        return id;
    }

    public RadioDialog setOk(String positiveText) {
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setText(positiveText);
        return this;
    }

    public RadioDialog setClose() {
        btn_cancel.setVisibility(View.VISIBLE);
        btn_cancel.setText("Close");
        return this;
    }

    public RadioDialog show() {
        dialog.show();
        return this;
    }
}
