package com.intopros.stampfee.offerlist;

import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.stampfee.R;
import com.intopros.stampfee.imageviewer.ImageViewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

class SimpleOfferView extends RecyclerView.ViewHolder {

    View rootView;

    @BindView(R.id.iv_color_image)
    ImageView ivColorImage;
    @BindView(R.id.iv_forward)
    ImageView ivForward;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_scheme)
    TextView tvScheme;
    @BindView(R.id.tv_sub_title)
    AppCompatTextView tvSubTitle;

    public SimpleOfferView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);
    }

    public void setTitle(String txt) {
        tvTitle.setText(txt);
    }

    public void setDescription(String txt) {
        tvSubTitle.setText(txt);
    }

    public void setOfferType(String txt) {
        tvScheme.setText(txt);
    }

    public void setImage(final String url) {
        if (TextUtils.isEmpty(url)) {
            ivForward.setVisibility(View.GONE);
        } else {
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rootView.getContext().startActivity(new Intent(rootView.getContext(), ImageViewActivity.class)
                            .putExtra("image", url));
                }
            });
        }

    }
}
