package com.intopros.stampfee.scan;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.profile.ProfileActivity;
import com.intopros.stampfee.utils.Utilities;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScanCountActivity extends AppCompatActivity {

    @BindView(R.id.numberslist)
    RecyclerView numberslist;
    @BindView(R.id.parentcntn)
    LinearLayout parentcntn;
    @BindView(R.id.billno)
    EditText billno;
    @BindView(R.id.view_service)
    LinearLayout view_service;
    @BindView(R.id.service_spinner)
    Spinner service_spinner;
    List<StampTypeObject> serviceList = null;
    StampTypeObject selectedService = null;

    GridLayoutManager layoutManager;
    ScanCountAdapter adapter;
    SyncDatabase syncdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_count);
        ButterKnife.bind(this);
        syncdb = new SyncDatabase();

        ActivityCompat.requestPermissions(getActivityContext(), new
                String[]{Manifest.permission.CAMERA}, 20);

        layoutManager = new GridLayoutManager(this, 3);
        numberslist.setLayoutManager(layoutManager);
        adapter = new ScanCountAdapter(this);
        numberslist.setAdapter(adapter);

        if (getIntent().getBooleanExtra("billno", false)) {
            billno.setVisibility(View.VISIBLE);
        } else {
            billno.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(getIntent().getStringExtra("scantype"))) {

            view_service.setVisibility(View.VISIBLE);
            serviceList = ServiceGenerator.getGson().fromJson(getIntent().getStringExtra("scantype"), new TypeToken<List<StampTypeObject>>() {
            }.getType());
            serviceList.add(0, new StampTypeObject("Select Service","-1"));
            ArrayAdapter<StampTypeObject> dataAdapter = new ArrayAdapter<>(getActivityContext(), android.R.layout.simple_spinner_item, serviceList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            service_spinner.setAdapter(dataAdapter);
            service_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0)
                        selectedService = serviceList.get(position);
                    else
                        selectedService = null;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else {
            view_service.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.close)
    public void close() {
        finish();
    }

    @OnClick(R.id.scan)
    public void scan() {

        if (getIntent().getBooleanExtra("billno", false) && (billno.getText().toString().length() < 1)) {
            billno.setError("Please enter bill number.");
            Utilities.Toaster(getActivityContext(), "Please enter bill number.");
        } else if (serviceList != null && selectedService == null) {
            Utilities.Toaster(getActivityContext(), "Please select service.");
        } else {
            scanstamp();
        }

    }

    private Activity getActivityContext() {
        return this;
    }

    private void scanstamp() {
        String stampType = null;
        if (selectedService!=null)
            stampType = selectedService.getStamptype();

        new SetRequest().get(this).pdialog(false)
                .set(ServiceGenerator.createService().stamp(billno.getText().toString(),
                        (adapter.getPos() + 1) + "",
                        getIntent().getStringExtra("code"),
                        getPref(SharedValue.currentLong), getPref(SharedValue.currentLat),
                        stampType))
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
                        syncdb.increaseStamp(adapter.getPos() + 1);
                        parentcntn.setVisibility(View.GONE);
                        ResponseClass.StampResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.StampResponse.class);
                        new SmallDialog(ScanCountActivity.this, response.getMessage(), new OnDialogSelect() {
                            @Override
                            public void onSuccess() {
                                startActivity(new Intent(getActivityContext(), ProfileActivity.class));
                                getActivityContext().finish();
                            }

                            @Override
                            public void onCancel() {
                                getActivityContext().finish();
                            }
                        }).setno("CANCEL").setyes("OK").show();

                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        new SmallDialog(ScanCountActivity.this, err, new OnDialogSelect() {
                            @Override
                            public void onSuccess() {
                                getActivityContext().finish();
                            }

                            @Override
                            public void onCancel() {
                                getActivityContext().finish();
                            }
                        }).hideno().setyes("OK").show();

                    }
                });

    }

    //get String preference
    public String getPref(String key) {
        return SharedPreferencesHelper.getSharedPreferences(getActivityContext(), key, "");
    }

    //set String preference
    public void setPref(String key, String value) {
        SharedPreferencesHelper.setSharedPreferences(getActivityContext(), key, value);
    }
}

