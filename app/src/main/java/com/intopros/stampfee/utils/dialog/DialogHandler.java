package com.intopros.stampfee.utils.dialog;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;

import java.util.ArrayList;
import java.util.List;

public class DialogHandler {

    DialogButtons buttons = new DialogButtons();
    int pos = 0;
    List<DialogResponse> dialog = new ArrayList<>();
    Activity a;
    RatingDialog rating;
    EditTextDialog input;
    RadioDialog radio;
    CheckDialog checkbox;

    public void openDialog(Activity activity, List<DialogResponse> mdialog) {
        dialog = mdialog;
        a = activity;
        if (dialog.size() > pos) {
            buttons = dialog.get(pos).getButtons();
            switch (dialog.get(pos).getType()) {
                case "input":
                    input = new EditTextDialog(activity, actions);
                    input.setTitle(dialog.get(pos).getTitle());
                    input.setHint(dialog.get(pos).getDescription());
                    if (buttons.getOk())
                        input.setOk(buttons.getButtontext());
                    if (buttons.getCancel())
                        input.setClose();
                    input.show();

                    break;
                case "radio":
                    radio = new RadioDialog(activity, actions);
                    radio.setTitle(dialog.get(pos).getTitle());
                    radio.addRadioButtons(dialog.get(pos).getOptions());
                    if (buttons.getOk())
                        radio.setOk(buttons.getButtontext());
                    if (buttons.getCancel())
                        radio.setClose();
                    radio.show();
                    break;
                case "rating":
                    rating = new RatingDialog(activity, actions);
                    rating.setTitle(dialog.get(pos).getTitle());
                    if (buttons.getOk())
                        rating.setOk(buttons.getButtontext());
                    if (buttons.getCancel())
                        rating.setClose();
                    rating.show();
                    break;
                case "image":
                    ImageDialog image = new ImageDialog(activity, actions);
                    image.setTitle(dialog.get(pos).getTitle());
                    image.setImage(dialog.get(pos).getImage());
                    if (dialog.get(pos).getDescription() != null)
                        image.setDescription(dialog.get(pos).getDescription());
                    if (buttons.getOk())
                        image.setOk(buttons.getButtontext());
                    if (buttons.getCancel())
                        image.setClose();
                    image.show();
                    break;
                case "text":
                    TextDialog text = new TextDialog(activity, actions);
                    text.setTitle(dialog.get(pos).getTitle());
                    if (buttons.getOk())
                        text.setOk(buttons.getButtontext());
                    if (buttons.getCancel())
                        text.setClose();
                    text.show();
                    break;
                case "checkbox":
                    checkbox = new CheckDialog(activity, actions);
                    checkbox.setTitle(dialog.get(pos).getTitle());
                    checkbox.setCheckBox(dialog.get(pos).getOptions());
                    if (buttons.getOk())
                        checkbox.setOk(buttons.getButtontext());
                    if (buttons.getCancel())
                        checkbox.setClose();
                    checkbox.show();
                    break;
            }
            pos++;
        }
    }

    OnDialogButtonSelect actions = new OnDialogButtonSelect() {
        @Override
        public void onSuccess() {
//            Log.d("ta", "onSuccess: " + pos);
            String responsetext = gettext(dialog.get(pos - 1));

            new SetRequest().get(a).pdialog(true).set(ServiceGenerator.createService()
                    .setDialog(dialog.get(pos - 1).getDialogid(), responsetext)).start(new OnRes() {
                @Override
                public void OnSuccess(JsonObject res) {
                    openDialog(a, dialog);
                }

                @Override
                public void OnError(String err, String errcode, int code, JsonObject res) {
                    Toast.makeText(a, err, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onCancel() {
            openDialog(a, dialog);

        }

        @Override
        public void onSkip() {
            openDialog(a, dialog);

        }
    };

    private String gettext(DialogResponse dialogResponse) {
        switch (dialogResponse.getType().toLowerCase()) {
            case "input":
                return input.getFeedback();
            case "radio":
                return radio.getSelectedRadioValue(dialogResponse.getOptions()) + "";
            case "rating":
                return rating.getRating() + "";
            case "checkbox":
                return checkbox.getids(dialogResponse.getOptions());
            default:
                return null;
        }
    }

}
