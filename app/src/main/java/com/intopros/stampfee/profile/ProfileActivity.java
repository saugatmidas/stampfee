package com.intopros.stampfee.profile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.JsonObject;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.notification.NotificationActivity;
import com.intopros.stampfee.qrreader.BarcodeReaderActivity;
import com.intopros.stampfee.utils.Utilities;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class ProfileActivity extends AbstractActivity {


    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    ProfileAdapter mAdapter;
    @BindView(R.id.errorblock)
    RelativeLayout errorblock;
    @BindView(R.id.errmsg)
    TextView errmsg;

    ArrayList<HomeModel> mlist = new ArrayList<>();
    GridLayoutManager layoutManager;

    @Override
    public int setLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Profile", true);
        layoutManager = new GridLayoutManager(getActivityContext(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (mAdapter.getItemViewType(position) == 2) {
                    return 1;
                }
                return 2;
            }
        });
        recycler_view.setLayoutManager(layoutManager);
        OverScrollDecoratorHelper.setUpOverScroll(recycler_view, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        mAdapter = new ProfileAdapter(this, mlist);
        recycler_view.setAdapter(mAdapter);
        loadData(null);

    }


    private void requestData() {
        new SetRequest().get(getActivityContext()).pdialog(false).set(ServiceGenerator.createService()
                .getprofile("profile")).start(new OnRes() {
            @Override
            public void OnSuccess(JsonObject res) {
                ResponseClass.ProfileResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.ProfileResponse.class);
                loadData(response.getResponse());
            }

            @Override
            public void OnError(String err, String errcode, int code, JsonObject res) {
                if (code != 204) {
                    errorblock.setVisibility(View.VISIBLE);
                    errmsg.setText(err);
                }
            }
        });

    }

    private void loadData(List<StampTable> response) {
        mlist.clear();
        mlist.add(new HomeModel("profile"));
        if (response != null) {
            mlist.add(new HomeModel("title").setTitle("My Stamps"));
            for (int i = 0; i < response.size(); i++) {
                mlist.add(new HomeModel("stamp", response.get(i)));
            }
        }
        mAdapter.notifyDataSetChanged();
    }


    @OnClick(R.id.notification)
    public void notification() {
        startActivity(new Intent(this, NotificationActivity.class));
    }


    @OnClick(R.id.errbtn)
    public void errClick() {
        errorblock.setVisibility(View.GONE);
        requestData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestData();
    }

    private final int CLAIM = 199;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 24:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /* takePicture(); */
                    Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(getActivityContext(), true, false);
                    startActivityForResult(launchIntent, CLAIM);
                } else {
                    Toast.makeText(getActivityContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

}
