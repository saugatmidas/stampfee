package com.intopros.stampfee;

import android.app.Activity;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;

import com.google.gson.JsonObject;
import com.intopros.stampfee.base.AbstractActivity;
import com.intopros.stampfee.base.Goto;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.update.UpdateActivity;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;

public class SplashActivity extends AbstractActivity {

    @BindView(R.id.view_loading)
    AVLoadingIndicatorView view_loading;

    @Override
    public int setLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Login", false);
        if (getIntPref("minversion") > BuildConfig.VERSION_CODE) {
            startActivity(new Intent(getActivityContext(), UpdateActivity.class).putExtra("skip", false));
            finish();
        } else if (new SyncDatabase().getApiToken().isEmpty()) {
            view_loading.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new Goto().login(getActivityContext());
                    finish();
                }
            }, 500);
        } else {
            checkUpdate();
        }
    }

    public void hashFromSHA1(String sha1) {
        String[] arr = sha1.split(":");
        byte[] byteArr = new  byte[arr.length];

        for (int i = 0; i< arr.length; i++) {
            byteArr[i] = Integer.decode("0x" + arr[i]).byteValue();
        }

        Log.e("hash : ", Base64.encodeToString(byteArr, Base64.NO_WRAP));
    }

    private void checkUpdate() {
        new SetRequest().get(getActivityContext())
                .set(ServiceGenerator.createService().checkUpdate())
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
                        ResponseClass.UserResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.UserResponse.class);

                        new SyncDatabase().saveUserData(response.getResponse());
                        new SyncDatabase().savefullStampCount(response.getResponse().getUserid(), response.getExtraparams().getStamps(), response.getExtraparams().getReedems(), response.getExtraparams().getRedeemavailable(), response.getExtraparams().getOffersavailable());
                        new SyncDatabase().saveBanner(response.getExtraparams().getBanners());
                        try {
                            SharedPreferencesHelper.setSharedPreferences(getApplicationContext(), "minversion", response.getExtraparams().getMinversion());
                        } catch (Exception e) {

                        }

                        try {
                            SharedPreferencesHelper.setSharedPreferences(getApplicationContext(), SharedValue.showDialog, response.getExtraparams().getShowdialog());
                        } catch (Exception e) {

                        }

                        if (response.getExtraparams().getForceupdate().trim().equals("1") || response.getExtraparams().getForceupdate().trim().equals("2")) {
                            startActivity(new Intent(getActivityContext(), UpdateActivity.class).putExtra("skip", response.getExtraparams().getForceupdate().trim().equals("1") ? true : false));
                            finish();
                        } else {
                            new Goto().home(getActivityContext());
                            finish();
                        }

                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        new Goto().home(getActivityContext());
                        finish();
                    }
                });

    }

}
