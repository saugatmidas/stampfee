package com.intopros.stampfee.profile;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intopros.stampfee.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class StampDetailView extends RecyclerView.ViewHolder {

    View rView;

    @BindView(R.id.stamps)
    RecyclerView stamps;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.desc)
    TextView desc;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.redeem)
    ImageView redeem;

    StampAdapter adapter;
    ArrayList<Boolean> mlist = new ArrayList<>();

    public StampDetailView(View view) {
        super(view);
        this.rView = view;
        ButterKnife.bind(this, rView);

        stamps.setLayoutManager(new GridLayoutManager(rView.getContext(), 3));

    }

    public void setUpStampts(String type) {
        adapter = new StampAdapter(rView.getContext(), mlist, type);
        stamps.setAdapter(adapter);
    }

    public void setName(String txt) {
        name.setText(txt);
    }

    public void setDesc(String txt) {
        desc.setText(txt);
    }

    public void setAddress(String txt) {
        address.setText(txt);
    }

    public void setTotal(int total, int stamp) {
        mlist.clear();
        for (int i = 0; i < total; i++) {
            if (stamp > i) {
                mlist.add(true);
            } else {
                mlist.add(false);
            }
        }
    }

    public void isRedeemAvailable(Boolean is) {
        if (is) {
            desc.setTextColor(ContextCompat.getColor(rView.getContext(), R.color.homerate));
            redeem.setColorFilter(ContextCompat.getColor(rView.getContext(), R.color.homerate), android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            desc.setTextColor(ContextCompat.getColor(rView.getContext(), R.color.icon_grey));
            redeem.setColorFilter(ContextCompat.getColor(rView.getContext(), R.color.icon_grey), android.graphics.PorterDuff.Mode.SRC_IN);
        }

    }


}
