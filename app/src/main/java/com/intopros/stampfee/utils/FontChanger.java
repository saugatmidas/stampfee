package com.intopros.stampfee.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class FontChanger {

    public static void overrideFont(Context context, String typeface, String font) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(), font);
        replaceFont(typeface, regular);
    }

    private static void replaceFont(String typeface, Typeface regular) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Map<String, Typeface> newMap = new HashMap<>();
            newMap.put("serif", regular);
            try {
                final Field staticField = Typeface.class
                        .getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                staticField.set(null, newMap);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                final Field staticField = Typeface.class
                        .getDeclaredField(typeface);
                staticField.setAccessible(true);
                staticField.set(null, regular);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                Log.e(FontChanger.class.getSimpleName(), "Can not set custom font " + regular + " instead of " + typeface);
            }
        }
    }
}
