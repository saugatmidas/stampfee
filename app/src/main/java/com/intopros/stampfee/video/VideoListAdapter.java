package com.intopros.stampfee.video;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.database.VideoTable;
import com.intopros.stampfee.video.player.YoutubePlayer;

import java.util.List;

public class VideoListAdapter extends AbstractAdapter<VideoTable> {

    Boolean opennew = true;

    public VideoListAdapter(Activity activity, Boolean opennew, List<VideoTable> mlist) {
        this.activity = activity;
        this.opennew = opennew;
        this.mItemList = mlist;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_video_list, parent, false);
        return new VideoListHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);
        if (viewHolder instanceof VideoListHolder) {
            VideoListHolder holder = (VideoListHolder) viewHolder;
            holder.setImage(mItemList.get(position).getThumbnail());
            holder.setTitle(mItemList.get(position).getTitle(), mItemList.get(position).getDescription(),
                    mItemList.get(position).getCredit(), mItemList.get(position).getVideoduration());
            holder.rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (opennew) {
                        activity.startActivity(new Intent(activity, YoutubePlayer.class)
                                .putExtra("ctitle", mItemList.get(position).getTitle())
                                .putExtra("credit", mItemList.get(position).getCredit())
                                .putExtra("subid", activity.getIntent().getStringExtra("id"))
                                .putExtra("url", mItemList.get(position).getLink())
                                .putExtra("vidid", mItemList.get(position).getVideoid())
                        );
                    } else {
                        ((YoutubePlayer) activity).playVideo(mItemList.get(position).getLink(), activity.getIntent().getStringExtra("subid"),
                                String.valueOf(mItemList.get(position).getVideoid()), mItemList.get(position).getTitle(), mItemList.get(position).getCredit());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

}