package com.intopros.stampfee.dashboard.videorecipe;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractAdapter;
import com.intopros.stampfee.database.VideoTable;
import com.intopros.stampfee.video.player.YoutubePlayer;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HorizontalRecipeAdapter extends AbstractAdapter<VideoTable> {

    public HorizontalRecipeAdapter(Activity activity, List<VideoTable> list) {
        this.activity = activity;
        this.mItemList = list;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recipe, parent, false);
        return new RecipeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder viewHolder, final int position) {
        RecipeHolder holder = (RecipeHolder) viewHolder;
        holder.setItem(mItemList.get(holder.getAdapterPosition()));
        holder.rView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, YoutubePlayer.class)
                        .putExtra("ctitle", mItemList.get(position).getTitle())
                        .putExtra("credit", mItemList.get(position).getCredit())
                        .putExtra("subid", activity.getIntent().getStringExtra("id"))
                        .putExtra("url", mItemList.get(position).getLink())
                        .putExtra("vidid", mItemList.get(position).getVideoid()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
