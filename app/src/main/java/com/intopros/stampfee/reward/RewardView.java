package com.intopros.stampfee.reward;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RewardView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.remaining)
    TextView remaining;
    @BindView(R.id.btn_claim)
    public TextView btn_claim;

    public RewardView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);

    }

    public void setName(String txt){
        tv_title.setText(txt);
    }
    public void setaddress(String txt){
        tv_address.setText(txt);
    }
    public void setDesc(String txt){
        tv_description.setText(txt);
    }
    public void setRemaining(String txt){
        remaining.setText(txt);
    }

}