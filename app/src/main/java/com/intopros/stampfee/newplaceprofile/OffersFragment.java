package com.intopros.stampfee.newplaceprofile;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractFragment;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.offerlist.OfferAdapter;
import com.intopros.stampfee.response.OffersObject;
import com.intopros.stampfee.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class OffersFragment extends AbstractFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    OfferAdapter mAdapter;
    List<OffersObject> mList = new ArrayList<>();

    @Override
    public Activity getActivityContext() {
        return getActivity();
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_offers;
    }

    @Override
    public void activityCreated() {
        if (getArguments() != null) {
            String list = getArguments().getString("list");
            mList.clear();
            mList = ServiceGenerator.getGson().fromJson(list, new TypeToken<List<OffersObject>>() {
            }.getType());
            if (mList != null) {
                for (OffersObject item : mList) {
                    item.setViewType(OfferAdapter.NO_IMAGE);
                }
            }
            recycler_view.setLayoutManager(new LinearLayoutManager(getActivityContext()));
            mAdapter = new OfferAdapter(mList);
            recycler_view.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setupOfferList(List<OffersObject> offerList) {
        mList.clear();
        mList.addAll(offerList);
        for (OffersObject item : mList) {
            item.setViewType(OfferAdapter.NO_IMAGE);
        }
        mAdapter.notifyDataSetChanged();
    }
}
