package com.intopros.stampfee.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.stampfee.R;

public class ImageDialog {

    Dialog dialog;
    TextView tv_title;
    TextView tv_description;
    ImageView iv_image;
    TextView btn_cancel;
    TextView btn_save;
    TextView btn_skip;

    Activity activity;
    OnDialogButtonSelect listener;

    public ImageDialog(Activity activity, OnDialogButtonSelect buttonListener) {
        this.activity = activity;
        this.listener = buttonListener;

        dialog = new Dialog(activity, R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image);
        dialog.setCancelable(false);


        tv_title = dialog.findViewById(R.id.tv_title);
        tv_description = dialog.findViewById(R.id.tv_description);
        iv_image = dialog.findViewById(R.id.iv_image);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        btn_skip = dialog.findViewById(R.id.btn_skip);

        tv_title.setVisibility(View.GONE);
        tv_description.setVisibility(View.GONE);
        btn_skip.setVisibility(View.GONE);
        btn_save.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSuccess();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onCancel();
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSkip();
            }
        });
    }

    public ImageDialog setDescription(String description) {
        tv_description.setText(description);
        tv_description.setVisibility(View.VISIBLE);
        return this;
    }

    public ImageDialog setImage(String url) {
        Glide.with(activity)
                .load(url)
                .apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_logo))
                .into(iv_image);
        return this;
    }

    public ImageDialog setTitle(String title) {
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(title);
        return this;
    }

    public ImageDialog setOk(String positiveText) {
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setText(positiveText);
        return this;
    }

    public ImageDialog setSkip(String skipText) {
        btn_skip.setText(skipText);
        btn_skip.setVisibility(View.VISIBLE);
        return this;
    }

    public ImageDialog show() {
        dialog.show();
        return this;
    }

    public ImageDialog setClose() {
        btn_cancel.setText("Close");
        btn_cancel.setVisibility(View.VISIBLE);
        return this;
    }
}
