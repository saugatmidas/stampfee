package com.intopros.stampfee.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.intopros.stampfee.R;

public class TextDialog {

    Dialog dialog;
    TextView tv_title;
    TextView tv_text;
    Button btn_cancel;
    Button btn_save;
    Button btn_skip;

    Activity activity;
    OnDialogButtonSelect listener;

    public TextDialog(Activity activity, OnDialogButtonSelect buttonListener) {
        this.activity = activity;
        listener = buttonListener;
        dialog = new Dialog(activity, R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_text);

        tv_title = dialog.findViewById(R.id.tv_title);
        tv_text = dialog.findViewById(R.id.tv_text);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        btn_skip = dialog.findViewById(R.id.btn_skip);

        tv_text.setVisibility(View.GONE);
        btn_skip.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_save.setVisibility(View.GONE);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSuccess();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onCancel();
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSkip();
            }
        });
    }

    public TextDialog setTitle(String title) {
        tv_title.setText(title);
        return this;
    }

    public TextDialog setText(String text) {
        tv_text.setVisibility(View.VISIBLE);
        tv_text.setText(text);
        return this;
    }


    public TextDialog setOk(String positiveText) {
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setText(positiveText);
        return this;
    }

    public TextDialog setClose() {
        btn_cancel.setVisibility(View.VISIBLE);
        btn_cancel.setText("Close");
        return this;
    }

    public TextDialog show() {
        dialog.show();
        return this;
    }
}
