package com.intopros.stampfee.dashboard.slider;

import com.intopros.stampfee.R;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class FlatSliderAdapter extends SliderAdapter {
    String[] banner;

    public FlatSliderAdapter(String[] banner) {
        this.banner = banner;
    }

    @Override
    public int getItemCount() {
        return banner.length;
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.bindImageSlide(banner[position], R.drawable.placeholder, R.drawable.placeholder);

    }
}