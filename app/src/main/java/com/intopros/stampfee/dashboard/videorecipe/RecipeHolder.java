package com.intopros.stampfee.dashboard.videorecipe;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.intopros.stampfee.R;
import com.intopros.stampfee.database.VideoTable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeHolder extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.tv_recipe_title)
    public TextView tv_recipe_title;
    @BindView(R.id.tv_timer)
    public TextView tv_timer;
    @BindView(R.id.tv_recipe_credit)
    public TextView tv_recipe_credit;
    @BindView(R.id.iv_recipe_thumb)
    public ImageView iv_recipe_thumb;

    public RecipeHolder(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setItem(VideoTable item) {
        tv_timer.setText(item.getVideoduration());
        tv_recipe_title.setText(item.getTitle());
        tv_recipe_credit.setText(item.getCredit());
        if (item.getThumbnail() != null)
            Glide.with(rView.getContext())
                    .load(item.getThumbnail())
                    .into(iv_recipe_thumb);
    }
}
