package com.intopros.stampfee.newplaceprofile;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.AbstractFragment;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.dashboard.HomeModel;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.profile.ProfileAdapter;
import com.intopros.stampfee.reward.RewardAdapter;
import com.intopros.stampfee.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class RewardsFragment extends AbstractFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    ProfileAdapter mAdapter;
    List<HomeModel> cardList = new ArrayList<>();

    @Override
    public Activity getActivityContext() {
        return getActivity();
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_rewards;
    }

    @Override
    public void activityCreated() {

        if (getArguments() != null) {
            String list = getArguments().getString("list");
            Utilities.Logger("redeemList1: " + list);
            List<StampTable> mList = ServiceGenerator.getGson().fromJson(list, new TypeToken<List<StampTable>>() {
            }.getType());
            cardList.clear();
            for (int i = 0; i < mList.size(); i++) {
                cardList.add(new HomeModel("stamp", mList.get(i)));
            }
            recycler_view.setLayoutManager(new GridLayoutManager(getActivityContext(), 1));
            mAdapter = new ProfileAdapter(getActivityContext(), cardList);
            recycler_view.setAdapter(mAdapter);
        }
    }

    public void setupRedeemList(List<StampTable> mList) {
        Utilities.Logger("setupRedeemList: " + ServiceGenerator.getGson().toJson(mList));
        cardList.clear();
        for (int i = 0; i < mList.size(); i++) {
            cardList.add(new HomeModel("stamp", mList.get(i)));
        }
        mAdapter.notifyDataSetChanged();
    }
}
