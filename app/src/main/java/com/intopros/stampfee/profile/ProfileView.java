package com.intopros.stampfee.profile;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.Goto;
import com.intopros.stampfee.database.StampCountTable;
import com.intopros.stampfee.database.SyncDatabase;
import com.intopros.stampfee.database.UserInfoTable;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileView extends RecyclerView.ViewHolder {

    public View rView;
    SyncDatabase syncdb;
    UserInfoTable user;
    StampCountTable stamps;

    @BindView(R.id.pimage)
    ImageView pimage;
    @BindView(R.id.ptext)
    TextView ptext;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.tv_stamp_count)
    TextView tv_stamp_count;
    @BindView(R.id.tv_redeem)
    TextView tv_redeem;

    public ProfileView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setProfile() {
        syncdb = new SyncDatabase();
        user = syncdb.getUserData();
        stamps = syncdb.getStampCount();
        name.setText(user.getName());
        email.setText(user.getEmail());
        tv_redeem.setText(stamps.getReedems() + "");
        tv_stamp_count.setText(stamps.getStamps() + "");
        if (user.getPhotourl() == null) {
            ptext.setText(user.getName());
            pimage.setImageDrawable(null);
        } else {
            ptext.setVisibility(View.GONE);
            Glide.with(rView.getContext())
                    .load(user.getPhotourl())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            ptext.setText(user.getName().substring(0, 1).toUpperCase());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .apply(new RequestOptions().circleCrop())
                    .into(pimage);
        }

    }

    @OnClick(R.id.logout)
    public void Logout() {
        new SmallDialog(rView.getContext(), "Are you sure you want to logout?", new OnDialogSelect() {
            @Override
            public void onSuccess() {
                new SyncDatabase().clearDatabase(rView.getContext());
                new Goto().login(rView.getContext());
            }

            @Override
            public void onCancel() {

            }
        }).show();
    }

}