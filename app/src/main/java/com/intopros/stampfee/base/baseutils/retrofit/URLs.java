package com.intopros.stampfee.base.baseutils.retrofit;

public class URLs {

    public static final String host = "http://ec2-13-235-13-19.ap-south-1.compute.amazonaws.com/api/";

    public static final String home = "home";
    public static final String checkupdate = "checkupdate";
    public static final String stamp = "stamp";
    public static final String scan = "scan";
    public static final String offers = "offers";
    public static final String profile = "profile";
    public static final String restroprofile = "restroprofile";
    public static final String restaurants = "restaurants";
    public static final String register = "register";
    public static final String dialog = "dialog";
    public static final String notifications = "notifications";
    public static final String redeem = "redeem";

}
