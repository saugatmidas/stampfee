package com.intopros.stampfee.dashboard.stamps;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestroStampView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.tv_redeem)
    public TextView tv_redeem;
    @BindView(R.id.tv_stamps)
    public TextView tv_stamps;
    @BindView(R.id.tv_cafe_name)
    public TextView tv_cafe_name;

    @BindView(R.id.tv_cafe_location)
    public TextView tv_cafe_location;
    @BindView(R.id.iv_cafe_image)
    ImageView iv_cafe_image;

    public RestroStampView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setImage(String img){
        Glide.with(rView.getContext())
                .load(img)
                .apply(new RequestOptions().centerCrop())
                .into(iv_cafe_image);
    }

}
