package com.intopros.stampfee.dashboard.stamps;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TitleView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.title)
    TextView title;

    public TitleView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setTitle(String txt){
        title.setText(txt);
    }

}
