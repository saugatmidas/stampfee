package com.intopros.stampfee.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class StampCountTable {

    @Unique
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("reedems")
    @Expose
    private Integer reedems;
    @SerializedName("stamps")
    @Expose
    private Integer stamps;
    @SerializedName("offersavailable")
    @Expose
    private Integer offersavailable;
    @SerializedName("redeemavailable")
    @Expose
    private Integer redeemavailable;

    public StampCountTable(String userid, Integer reedems, Integer stamps) {
        this.userid = userid;
        this.reedems = reedems;
        this.stamps = stamps;
    }

    @Generated(hash = 429917626)
    public StampCountTable(String userid, Integer reedems, Integer stamps,
            Integer offersavailable, Integer redeemavailable) {
        this.userid = userid;
        this.reedems = reedems;
        this.stamps = stamps;
        this.offersavailable = offersavailable;
        this.redeemavailable = redeemavailable;
    }
    @Generated(hash = 525342112)
    public StampCountTable() {
    }
    
    public String getUserid() {
        return this.userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
    public Integer getReedems() {
        return this.reedems;
    }
    public void setReedems(Integer reedems) {
        this.reedems = reedems;
    }
    public Integer getStamps() {
        return this.stamps;
    }
    public void setStamps(Integer stamps) {
        this.stamps = stamps;
    }
    public Integer getOffersavailable() {
        return this.offersavailable;
    }
    public void setOffersavailable(Integer offersavailable) {
        this.offersavailable = offersavailable;
    }
    public Integer getRedeemavailable() {
        return this.redeemavailable;
    }
    public void setRedeemavailable(Integer redeemavailable) {
        this.redeemavailable = redeemavailable;
    }


}
