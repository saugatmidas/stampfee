package com.intopros.stampfee.dashboard;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CircleView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.ptext)
    TextView ptext;
    @BindView(R.id.pimage)
    ImageView pimage;

    public CircleView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setTitle(String txt) {
        name.setText(txt);

        ptext.setText(txt);
    }

    public void setRandomColor(int pos) {
        switch (pos % 5) {
            case 1:
                setBackground(R.color.homerate);
                break;
            case 2:
                setBackground(R.color.g_blue);
                break;
            case 3:
                setBackground(R.color.g_red);
                break;
            case 4:
                setBackground(R.color.g_green);
                break;
            default:
                setBackground(R.color.icon_grey);

        }
    }

    public void setPlaceholder(String txt,String logo) {
//        if(logo==null) {
            ptext.setVisibility(View.VISIBLE);
            ptext.setText(txt);
            pimage.setImageDrawable(null);
//        }else{
//            ptext.setVisibility(View.GONE);
//            setBackground(R.color.white);
//            Glide.with(rView.getContext())
//                    .load(logo)
//                    .into(pimage);
//        }
    }

    public void setBackground(int color) {
//        ViewCompat.setBackgroundTintList(cntn,ColorStateList.valueOf(rView.getResources().getColor(color)));
        ColorStateList csl = AppCompatResources.getColorStateList(rView.getContext(), color);
        Drawable drawable = DrawableCompat.wrap(pimage.getBackground());
        DrawableCompat.setTintList(drawable, csl);
        pimage.setBackground(drawable);
    }


}