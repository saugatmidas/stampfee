package com.intopros.stampfee.profile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.JsonObject;
import com.intopros.stampfee.R;
import com.intopros.stampfee.base.baseutils.retrofit.OnRes;
import com.intopros.stampfee.base.baseutils.retrofit.ResponseClass;
import com.intopros.stampfee.base.baseutils.retrofit.ServiceGenerator;
import com.intopros.stampfee.base.baseutils.retrofit.SetRequest;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.stampfee.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.stampfee.database.StampTable;
import com.intopros.stampfee.qrreader.BarcodeReaderActivity;
import com.intopros.stampfee.utils.Utilities;
import com.intopros.stampfee.utils.dialog.EditTextDialog;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import java.util.Objects;

public class StampDetailDialog extends AppCompatActivity {

    public final static int STAMP_DIALOG = 9841;
    private final int CLAIM = 199;

    private Dialog dialog;
    private StampTable item;

    private ImageView ivClose;
    private ImageView pImage;
    private TextView pText;
    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvProgressMessage;
    private TextView tvRedeemedCount;
    private TextView tvPendingRedeemCount;
    private ProgressBar pbProgress;
    private Button btnRedeem;

    String billno = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_stamp_detail);
        item = ServiceGenerator.getGson().fromJson(getIntent().getStringExtra("stamp"), StampTable.class);
        initiateView();
        setImage(null);
        tvTitle.setText(item.getRestaurantname());
        if (!TextUtils.isEmpty(item.getDescription()))
            tvDescription.setText(item.getDescription());
        setProgress();
        setRedeems();
    }

    private void setRedeems() {
        tvRedeemedCount.setText(item.getRedeemed());
        tvPendingRedeemCount.setText(String.valueOf(item.getPending()));
        if (item.getPending() > 0) {
            btnRedeem.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(item.getDescription()))
                btnRedeem.setText(item.getDescription());
            btnRedeem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (item.getBill()) {
                        final EditTextDialog input = new EditTextDialog(getActivityContext(), null);
                        input.setTitle("Link reward to bill.");
                        input.btn_save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!TextUtils.isEmpty(input.getFeedback())) {
                                    billno = input.getFeedback();
                                    input.dialog.dismiss();
                                    ActivityCompat.requestPermissions(getActivityContext(), new
                                            String[]{Manifest.permission.CAMERA}, 24);
                                } else
                                    Utilities.Toaster(getActivityContext(), "Please enter bill number.");

                            }
                        });
                        input.setHint("Please enter bill number.");
                        input.setOk("Scan");
                        input.setClose();
                        input.show();
                    } else {
                        ActivityCompat.requestPermissions(getActivityContext(), new
                                String[]{Manifest.permission.CAMERA}, 24);
                    }
                }
            });
        }
    }

    private void setProgress() {
        pbProgress.setMax(item.getStampcount());
        pbProgress.setProgress(item.getCurrentstamps());
        int remainingStamps = item.getStampcount() - item.getCurrentstamps();
        StringBuilder message = new StringBuilder();
        message.append(remainingStamps);
        if (remainingStamps > 1)
            message.append(" stamps ");
        else
            message.append(" stamp ");
        message.append("away from your REWARD");
        tvProgressMessage.setText(message.toString());
    }

    private void setImage(String imageURL) {
        if (imageURL == null) {
            pText.setText(item.getRestaurantname().substring(0, 1).toUpperCase());
            pImage.setImageDrawable(null);
        } else {
            pText.setVisibility(View.GONE);
            Glide.with(dialog.getContext())
                    .load(imageURL)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pText.setText(item.getRestaurantname().substring(0, 1).toUpperCase());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .apply(new RequestOptions().circleCrop())
                    .into(pImage);
        }
    }

    private void initiateView() {
        dialog = new Dialog(getActivityContext(), R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_stamp_detail);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        ivClose = dialog.findViewById(R.id.iv_close);
        pImage = dialog.findViewById(R.id.pimage);
        pText = dialog.findViewById(R.id.ptext);
        tvTitle = dialog.findViewById(R.id.tv_title);
        tvDescription = dialog.findViewById(R.id.tv_description);
        tvProgressMessage = dialog.findViewById(R.id.tv_progress_message);
        tvRedeemedCount = dialog.findViewById(R.id.tv_redeemed_count);
        tvPendingRedeemCount = dialog.findViewById(R.id.tv_pending_redeem_count);
        pbProgress = dialog.findViewById(R.id.pb_progress);
        btnRedeem = dialog.findViewById(R.id.btn_redeem);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        dialog.show();
    }

    private Activity getActivityContext() {
        return this;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 24:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /* takePicture(); */
                    Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false);
                    startActivityForResult(launchIntent, CLAIM);
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == CLAIM && data != null) {
            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
            scanClaim(barcode.rawValue);
        }
    }

    private void scanClaim(String code) {
        new SetRequest().get(getActivityContext()).pdialog(false)
                .set(ServiceGenerator.createService().redeem(item.getStamptype(), billno, code,
                        SharedPreferencesHelper.getSharedPreferences(getApplicationContext(), SharedValue.currentLong, ""),
                        SharedPreferencesHelper.getSharedPreferences(getApplicationContext(), SharedValue.currentLat, "")))
                .start(new OnRes() {
                    @Override
                    public void OnSuccess(JsonObject res) {
                        ResponseClass.StringResponse response = ServiceGenerator.getGson().fromJson(res, ResponseClass.StringResponse.class);
                        new SmallDialog(getActivityContext(), response.getMessage(), new OnDialogSelect() {
                            @Override
                            public void onSuccess() {
                                Intent intent = new Intent();
                                setResult(RESULT_OK, intent);
                                finish();
                            }

                            @Override
                            public void onCancel() {

                            }
                        }).hideno().setCancelable(false).setyes("OK").show();
                    }

                    @Override
                    public void OnError(String err, String errcode, int code, JsonObject res) {
                        new SmallDialog(getActivityContext(), err, null).show();

                    }
                });
    }
}