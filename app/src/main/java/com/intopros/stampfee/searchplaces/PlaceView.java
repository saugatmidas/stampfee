package com.intopros.stampfee.searchplaces;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.stampfee.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class PlaceView extends RecyclerView.ViewHolder {

    View rootView;

    @BindView(R.id.iv_image_content)
    ImageView iv_image_content;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_sub_title)
    TextView tv_sub_title;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_scheme)
    TextView tv_scheme;

    public PlaceView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);
    }

    public void setName(String txt) {
        tv_title.setText(txt);
    }

    public void setDesc(String txt) {
        tv_description.setText(txt);
    }

    public void setAddress(String txt) {
        tv_sub_title.setText(txt);
    }

    public void setScheme(String txt) {
        tv_scheme.setText(txt);
    }

    public void hasOffer(Boolean has) {
        if (has) {
            tv_scheme.setVisibility(View.VISIBLE);
        } else {
            tv_scheme.setVisibility(View.GONE);
        }
    }

    public void setImage(String url) {
//        iv_image_content.setClipToOutline(true);
        Glide.with(rootView)
                .load(url)
                .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(16)))
                .into(iv_image_content);
    }
}
