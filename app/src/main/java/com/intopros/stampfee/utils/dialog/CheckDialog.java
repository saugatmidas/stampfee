package com.intopros.stampfee.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.intopros.stampfee.R;

import java.util.ArrayList;
import java.util.List;

public class CheckDialog {

    Dialog dialog;
    TextView tv_title;
    RadioGroup rg_radiogroup;
    TextView btn_cancel;
    TextView btn_save;
    TextView btn_skip;
    LinearLayout ll_cb_holder;

    Activity activity;
    OnDialogButtonSelect listener;

    public CheckDialog(Activity activity, OnDialogButtonSelect buttonListener) {
        this.activity = activity;
        this.listener = buttonListener;
        dialog = new Dialog(activity, R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_checkbox);
        dialog.setCancelable(false);

        tv_title = dialog.findViewById(R.id.tv_title);
        rg_radiogroup = dialog.findViewById(R.id.rg_radiogroup);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        btn_skip = dialog.findViewById(R.id.btn_skip);

        btn_skip.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_save.setVisibility(View.GONE);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSuccess();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onCancel();
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSkip();
            }
        });

    }

    public CheckDialog setTitle(String title) {
        tv_title.setText(title);
        return this;
    }

    public void setCheckBox(List<DialogOption> checkboxList) {
        ll_cb_holder = dialog.findViewById(R.id.ll_cb_holder);

        for (int i = 0; i < checkboxList.size(); i++) {
//            TableRow row = new TableRow(activity);
//            row.setId(i);
//            row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            CheckBox checkBox = new CheckBox(activity);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                }
            });
            checkBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            checkBox.setId(checkboxList.get(i).getValue());
            checkBox.setText(checkboxList.get(i).getLabel());
//            row.addView(checkBox);
            ll_cb_holder.addView(checkBox);
        }
    }

    public String getids(List<DialogOption> checkboxList) {
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < checkboxList.size(); i++) {
            if (((CheckBox) ll_cb_holder.getChildAt(i)).isChecked()) {
                ids.add(checkboxList.get(i).getValue() + "");
            }
        }
        return TextUtils.join(",", ids);
    }

    public CheckDialog setOk(String positiveText) {
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setText(positiveText);
        return this;
    }

    public CheckDialog setSkip(String skipText) {
        btn_skip.setText(skipText);
        btn_skip.setVisibility(View.VISIBLE);
        return this;
    }

    public void setClose() {
        btn_cancel.setVisibility(View.VISIBLE);
        btn_cancel.setText("Close");
    }

    public CheckDialog show() {
        dialog.show();
        return this;
    }
}
