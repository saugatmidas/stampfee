package com.intopros.stampfee.dashboard.slider;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.intopros.stampfee.R;
import com.intopros.stampfee.imageviewer.ImageViewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import ss.com.bannerslider.Slider;
import ss.com.bannerslider.event.OnSlideClickListener;

public class FlatImageSliderView extends RecyclerView.ViewHolder {


    View rootView;
    @BindView(R.id.imgslider)
    Slider imgslider;


    public FlatImageSliderView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);

    }

    public void setImages(final String[] mlist){
        imgslider.setAdapter(new FlatSliderAdapter(mlist));
        if(mlist.length>0) {
            imgslider.setOnSlideClickListener(new OnSlideClickListener() {
                @Override
                public void onSlideClick(int position) {
                    try {
                        rootView.getContext().startActivity(new Intent(rootView.getContext(), ImageViewActivity.class)
                                .putExtra("image", mlist[position]));
                    } catch (Exception e) {
                        rootView.getContext().startActivity(new Intent(rootView.getContext(), ImageViewActivity.class)
                                .putExtra("image", mlist[0]));
                    }
                }
            });
        }
    }
}
