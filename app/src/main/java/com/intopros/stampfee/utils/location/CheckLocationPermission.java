package com.intopros.stampfee.utils.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.intopros.stampfee.utils.PermissionUtils;
import com.intopros.stampfee.utils.Utilities;
import com.intopros.stampfee.utils.dialog.OnDialogSelect;
import com.intopros.stampfee.utils.dialog.SmallDialog;

import org.jetbrains.annotations.NotNull;

public class CheckLocationPermission implements LocationListener {

    Activity activity;
    public static final int REQUEST_FINE_LOCATION_PERMISSION = 30;
    private LocationManager locationManager;
    ProgressDialog dialog;
    private final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private final long MIN_TIME_BW_UPDATES = 1000 * 10 * 1;
    private SetLocationListener listener;

    public interface SetLocationListener {
        void onLocationReceived(Location location);
    }

    private Activity getActivityContext() {
        return activity;
    }

    public CheckLocationPermission(@NotNull Activity activity, SetLocationListener listener) {
        this.activity = activity;
        this.listener = listener;
        dialog = new ProgressDialog(getActivityContext());
        dialog.setCancelable(false);
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        checkPermission();
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivityContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //permission not granted
            //show explanation?
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivityContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                //no explanation needed initial request
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (PermissionUtils.neverAskAgainSelected(getActivityContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                        displayNeverAskAgainDialog();
                    } else {
                        ActivityCompat.requestPermissions(getActivityContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION_PERMISSION);
                    }
                }
            } else {
                //show explanation
                new SmallDialog(getActivityContext(), "We need to access location for performing necessary task. Please grant location permission to continue.", new OnDialogSelect() {
                    @Override
                    public void onSuccess() {
                        ActivityCompat.requestPermissions(getActivityContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION_PERMISSION);
                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
            }
        } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //permission granted but GPS is disabled
            new SmallDialog(getActivityContext(), "Location access is needed to scan the QR code. Please enable GPS.", new OnDialogSelect() {
                @Override
                public void onSuccess() {
                    getActivityContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }

                @Override
                public void onCancel() {

                }
            }).show();
        } else {
            //permission granted and GPS is enabled
            startLocationUpdates();
        }
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        dialog.setTitle("Fetching location");
        dialog.setMessage("Please wait for a while, it might take few minutes ... ");
        dialog.show();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
    }

    private void displayNeverAskAgainDialog() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivityContext());
        builder.setMessage("We need to access location for performing necessary task. Please permit the permission and try again through "
                + "Settings screen.\n\nSelect Permissions -> Enable permission");
        builder.setCancelable(false);
        builder.setPositiveButton("Permit Manually", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivityContext().getPackageName(), null);
                intent.setData(uri);
                getActivityContext().startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        listener.onLocationReceived(location);
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
        if (dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Utilities.Logger("onProviderEnabled");
        startLocationUpdates();
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

}
